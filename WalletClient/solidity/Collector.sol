pragma solidity ^0.4.14;

/**
 * Contract that exposes the needed Forwarder functions
 */
contract ForwarderInterface {
    // Forward a token
    function forwardToken(address tokenContractAddress);
    // Forward more than one tokens
    function forwardTokens(address[] tokenContractAddresses);
    // Foward Ether
    function forward();
}

/**
 * Contract that will collect multiple Forwarders
 */
contract Collector {

    /**
     * Collect a token from multiple Forwarders
     */
    function forwardToken(address tokenContractAddress, address[] forwarderAddresses) {
        for (uint i = 0; i < forwarderAddresses.length; i++) {
            var forwarderAddress = forwarderAddresses[i];
            ForwarderInterface instance = ForwarderInterface(forwarderAddress);
            instance.forwardToken(tokenContractAddress);
        }
    }

    /**
     * Collect more than one tokens from multiple Forwarders
     */
    function forwardTokens(address[] tokenContractAddresses, address[] forwarderAddresses) {
        for (uint i = 0; i < forwarderAddresses.length; i++) {
            var forwarderAddress = forwarderAddresses[i];
            ForwarderInterface instance = ForwarderInterface(forwarderAddress);
            instance.forwardTokens(tokenContractAddresses);
        }
    }

    /**
     * Collect Ether from multiple Forwarders
     */
    function forward(address[] forwarderAddresses) {
        for (uint i = 0; i < forwarderAddresses.length; i++) {
            var forwarderAddress = forwarderAddresses[i];
            ForwarderInterface instance = ForwarderInterface(forwarderAddress);
            instance.forward();
        }
    }

}