pragma solidity ^0.4.14;

/**
 * Contract that exposes the needed erc20 token functions
 */
contract ERC20Interface {
  // Send _value amount of tokens to address _to
  function transfer(address _to, uint256 _value) returns (bool success);
  // Get the account balance of another account with address _owner
  function balanceOf(address _owner) constant returns (uint256 balance);
}

/**
 * Contract that will forward any incoming Ether to a destination address
 */
contract Forwarder {
  // Address to which any funds sent to this contract will be forwarded
  address public destinationAddress;

  event ForwardEther(address from, uint value);
  event ForwardToken(address tokenContractAddress, uint256 value);

  /**
   * Create the contract, and set the destination address
   */
  function Forwarder(address collectionAddress) {
    destinationAddress = collectionAddress;
  }

  /**
   * Default function;
   * Gets called when Ether is deposited, and forwards it to the destination address
   */
  function() payable {
    if (!destinationAddress.call.value(msg.value)(msg.data))
      throw;
    // Fire off the deposited event if we can forward it
    ForwardEther(msg.sender, msg.value);
  }

  /**
   * Forward Ether to the destination address
   * Note: normally there is no need to forward Ether
   */
  function forward() {
    if (!destinationAddress.call.value(this.balance)())
      throw;
    ForwardEther(address(this), this.balance);
  }

  /**
   * Forward a token to the destination address
   * @param tokenContractAddress the address of the erc20 token contract
   */
  function forwardToken(address tokenContractAddress) {
    ERC20Interface instance = ERC20Interface(tokenContractAddress);
    var forwarderAddress = address(this);
    uint256 forwarderBalance = instance.balanceOf(forwarderAddress);
    if (forwarderBalance == 0) {
      return;
    }
    uint256 prevBalance = instance.balanceOf(destinationAddress);
    if (!instance.transfer(destinationAddress, forwarderBalance)) {
      throw;
    }
    uint256 currBalance = instance.balanceOf(destinationAddress);
    if (forwarderBalance != currBalance - prevBalance) {
      throw;
    }
    ForwardToken(tokenContractAddress, forwarderBalance);
  }

  /**
   * Forward more than one tokens to the destination address
   * @param tokenContractAddresses the addresses of the erc20 token contract
   */
  function forwardTokens(address[] tokenContractAddresses) {
    var forwarderAddress = address(this);
    for (uint i = 0; i < tokenContractAddresses.length; i++) {
        var tokenContractAddress = tokenContractAddresses[i];
        forwardToken(tokenContractAddress);
    }
  }

}