package com.funchain.wallet.eth;

import com.funchain.wallet.key.KeyCache;
import com.funchain.wallet.key.KeyCacheFactory;
import com.funchain.wallet.key.KeyRecord;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.nio.charset.Charset;

@RunWith(SpringRunner.class)
public class EthTxSignerTest {

    @Test
    public void testCreateContract() throws Exception {
        String pubKey = "56dDC11f890997AF41892a8f35eB36E7b770003F";

        KeyCache keyCache = KeyCacheFactory.getKeyCache(60, 0);
        KeyRecord keyRecord = new KeyRecord();
        keyRecord.setAddressIndex("0");
        keyRecord.setPubKey(pubKey);
        keyRecord.setPrivKey("DFCBE266E811A36C71DEC1518BBB28F5411623946A445CB2354FE4E30DEC54B2");
        keyCache.addKeyRecord(keyRecord);

        byte chainId = 42;
        long nonce = 212;
        long gasPrice = 1_000_000_000L;
        long gasLimit = 650000;

        String data = loadContract("prod/ForwarderContract");
        data = data + "4a9676e2f51e0e4d5113956d436815627d619901";

        String rawTx = EthTxSigner.createContract(chainId, pubKey, nonce, gasPrice, gasLimit, data);
        System.out.println(rawTx);
    }

    public String loadContract(String filePath) throws Exception {
        // load contract
        File file = new File(filePath);
        String contract = FileUtils.readFileToString(file, Charset.defaultCharset());
        return contract;
    }

}
