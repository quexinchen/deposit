package com.funchain.wallet.usdt;

import com.funchain.wallet.btc.BtcUtxo;
import com.funchain.wallet.btc.BtcVout;
import com.funchain.wallet.key.KeyCache;
import com.funchain.wallet.key.KeyCacheFactory;
import com.funchain.wallet.key.KeyRecord;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class UsdtTxSignerTest {

    @Test
    public void testUsdt1() {
        int network = 1;

        List<BtcUtxo> vins = new ArrayList<>();
        KeyCache keyCache = KeyCacheFactory.getKeyCache(0, 0);

        BtcUtxo utxo1 = new BtcUtxo();
        utxo1.setTxid("62403d93cb3deef52f3c4021dadfb7711bd2be6c1e5c7926e2a82d587d59fcef");
        utxo1.setN(1);
        //utxo1.setPubKey("02cb4c5e795e5d408247f5bcac6ad3a29baaf7176ee713ded9bcacc1f630ea48f1");
        utxo1.setRedeemScript("76a91486a8589ebcd788bc80428338e5bd89cf54802f5888ac");
        vins.add(utxo1);

        KeyRecord key1 = new KeyRecord();
        key1.setAddressIndex("30");
        key1.setPubKey("02cb4c5e795e5d408247f5bcac6ad3a29baaf7176ee713ded9bcacc1f630ea48f1");
        key1.setPrivKey("9f4f4ce65b8ea53220f30ae5213d776515e4289d5a560217b03c017f1e711139");
        keyCache.addKeyRecord(key1);

        BtcUtxo utxo2 = new BtcUtxo();
        utxo2.setTxid("9d3b851eeaed62423dc515da5d8f6e3aa9bf1b1249cf3188d1356527f4b3fcb7");
        utxo2.setN(1);
        //utxo2.setPubKey("032f0c2b033bcf6e7002f75c80f98ff08ae0eb6f836fa7cd8a5e462703fb03e56c");
        utxo2.setRedeemScript("76a914f2b9a15e884c678b830b7f3171f3986b64d183d688ac");
        vins.add(utxo2);

        KeyRecord key2 = new KeyRecord();
        key2.setAddressIndex("80");
        key2.setPubKey("032f0c2b033bcf6e7002f75c80f98ff08ae0eb6f836fa7cd8a5e462703fb03e56c");
        key2.setPrivKey("4f3613408bb21afafb5b94537a6bdb586402eb541ed773169d74eef26b7087b6");
        keyCache.addKeyRecord(key2);

        UsdtVout usdtVout = new UsdtVout();
        usdtVout.setAddress("miYiKnNBevLkvv5LaeLUNDFzFmNsFCjgQP");
        usdtVout.setDust(546L);
        usdtVout.setAmount(100_0000_0000L);

        BtcVout btcVout = new BtcVout();
        btcVout.setAddress("n3eNCJLXU1vx5rqJ5QXCrzP25AUYy4WcCq");
        btcVout.setAmount(1000_0000L - 500L);

        String rawTx = UsdtTxSigner.signTx(network, vins, usdtVout, btcVout);
        System.out.println(rawTx);
    }

    @Test
    public void testUsdt2() {
        int network = 1;

        List<BtcUtxo> vins = new ArrayList<>();
        KeyCache keyCache = KeyCacheFactory.getKeyCache(0, 0);

        BtcUtxo utxo1 = new BtcUtxo();
        utxo1.setTxid("a58e9e25ff57c8229dec7ccb97c912fbb1b054f9fe9eb6f6c77e221e8275eccc");
        utxo1.setN(2);
        //utxo1.setPubKey("02cb4c5e795e5d408247f5bcac6ad3a29baaf7176ee713ded9bcacc1f630ea48f1");
        utxo1.setRedeemScript("76a91486a8589ebcd788bc80428338e5bd89cf54802f5888ac");

        KeyRecord key1 = new KeyRecord();
        key1.setAddressIndex("30");
        key1.setPubKey("02cb4c5e795e5d408247f5bcac6ad3a29baaf7176ee713ded9bcacc1f630ea48f1");
        key1.setPrivKey("9f4f4ce65b8ea53220f30ae5213d776515e4289d5a560217b03c017f1e711139");
        keyCache.addKeyRecord(key1);

        BtcUtxo utxo2 = new BtcUtxo();
        utxo2.setTxid("c741d14924bfc4fdb535d4a4b197b884d0e8de0e268327608ad03446e7ee5c1b");
        utxo2.setN(0);
        //utxo2.setPubKey("032f0c2b033bcf6e7002f75c80f98ff08ae0eb6f836fa7cd8a5e462703fb03e56c");
        utxo2.setRedeemScript("76a914f2b9a15e884c678b830b7f3171f3986b64d183d688ac");

        KeyRecord key2 = new KeyRecord();
        key2.setAddressIndex("80");
        key2.setPubKey("032f0c2b033bcf6e7002f75c80f98ff08ae0eb6f836fa7cd8a5e462703fb03e56c");
        key2.setPrivKey("4f3613408bb21afafb5b94537a6bdb586402eb541ed773169d74eef26b7087b6");
        keyCache.addKeyRecord(key2);

        UsdtVout usdtVout = new UsdtVout();
        usdtVout.setAddress("miYiKnNBevLkvv5LaeLUNDFzFmNsFCjgQP");
        usdtVout.setDust(546L);
        usdtVout.setAmount(100_0000_0000L);

        BtcVout btcVout = new BtcVout();
        btcVout.setAddress("n3eNCJLXU1vx5rqJ5QXCrzP25AUYy4WcCq");
        btcVout.setAmount(999_9500L - 500L);

        vins.add(utxo2);
        vins.add(utxo1);

        String rawTx = UsdtTxSigner.signTx(network, vins, usdtVout, btcVout);
        System.out.println(rawTx);
    }

}
