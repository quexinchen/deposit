package com.funchain.wallet.usdt;

import com.funchain.wallet.ClientConfig;
import com.funchain.wallet.key.KeyCache;
import com.funchain.wallet.key.KeyCacheFactory;
import com.funchain.wallet.key.KeyRecord;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * USDT
 *
 * @author Dev
 */
@RunWith(SpringRunner.class)
public class UsdtActionTest {

    @Test
    public void testForward() {
        ClientConfig.serverUrl = "http://localhost:18080";
        ClientConfig.network = 1;
        ClientConfig.usdtFeePubKey = "032f0c2b033bcf6e7002f75c80f98ff08ae0eb6f836fa7cd8a5e462703fb03e56c";

        KeyCache keyCache = KeyCacheFactory.getKeyCache(0, 0);

        KeyRecord key1 = new KeyRecord();
        key1.setAddressIndex("30");
        key1.setPubKey("02cb4c5e795e5d408247f5bcac6ad3a29baaf7176ee713ded9bcacc1f630ea48f1");
        key1.setPrivKey("9f4f4ce65b8ea53220f30ae5213d776515e4289d5a560217b03c017f1e711139");
        keyCache.addKeyRecord(key1);

        KeyRecord key2 = new KeyRecord();
        key2.setAddressIndex("80");
        key2.setPubKey("032f0c2b033bcf6e7002f75c80f98ff08ae0eb6f836fa7cd8a5e462703fb03e56c");
        key2.setPrivKey("4f3613408bb21afafb5b94537a6bdb586402eb541ed773169d74eef26b7087b6");
        keyCache.addKeyRecord(key2);

        KeyRecord key3 = new KeyRecord();
        key3.setAddressIndex("305");
        key3.setPubKey("0274a2a58354293ec265ddfb8382eb21f748f2095d4c767bcdc5d33fa7dd426f2e");
        key3.setPrivKey("be70a8973826ba9d4227c496faa5491383a5399ed6f9b813a1c4e24353b01a91");
        keyCache.addKeyRecord(key3);

        ForwardArg arg = new ForwardArg();
        arg.setFeeRate(1);
        arg.setMin(100);
        arg.setDestinationAddr("mmbfaT8ZyDiij7nT1SnNGzNFsr3z58XvrH");

        UsdtAction.forward(arg);
    }

}
