package com.funchain.wallet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.funchain.wallet.btc.BtcUtxo;
import com.funchain.wallet.btc.BtcVout;
import com.funchain.wallet.usdt.UsdtVout;
import okhttp3.*;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 客户端
 *
 * @author Dev
 */
public class ClientApi {
    public static final long SEND_TIMEOUT = 30L;
    private static final long SLOW_NETWORK = 2000L;

    public static final int OK_CODE = 200;
    public static final int ERR_CODE = 500;

    private OkHttpClient httpClient;

    private static ClientApi INSTANCE;

    public static ClientApi getInstance() {
        if (null == INSTANCE) {
            INSTANCE = new ClientApi();
        }
        return INSTANCE;
    }

    private ClientApi() {
        OkHttpClient aHttpClient = new OkHttpClient();
        httpClient = aHttpClient.newBuilder()
                .connectTimeout(SEND_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(SEND_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    private Pair<String, Integer> sendRequest(OkHttpClient httpClient, Request request) {
        Response response = null;
        try {
            response = httpClient.newCall(request).execute();
            if (null == response) {
                System.out.println("Response error: NULL");
                return ImmutablePair.of(null, ERR_CODE);
            }

            if (OK_CODE != response.code()) {
                System.out.println("Response error: " + response.code());
                return ImmutablePair.of(null, response.code());
            }

            String responseBody = response.body().string();
            return ImmutablePair.of(responseBody, OK_CODE);
        } catch (Throwable e) {
            e.printStackTrace();
            return ImmutablePair.of(null, ERR_CODE);
        } finally {
            if (null != response) {
                response.close();
            }
        }
    }

    public String broadcastEthTx(String rawTx) {
        HttpUrl url = HttpUrl.parse(ClientConfig.serverUrl + "/broadcast/eth");
        String jsonStr = "{\"rawTx\":\"" + rawTx + "\"}";

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonStr);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        String txid = null;
        try {
            Pair<String, Integer> res = sendRequest(httpClient, request);
            if (OK_CODE == res.getRight()) {
                JSONObject jsonObject = JSON.parseObject(res.getLeft());
                txid = jsonObject.getString("txid");
            }
        }
        catch (Throwable e) {
            e.printStackTrace();
        }

        return txid;
    }

    public String broadcastBtcTx(String rawTx) {
        HttpUrl url = HttpUrl.parse(ClientConfig.serverUrl + "/broadcast/btc");
        String jsonStr = "{\"rawTx\":\"" + rawTx + "\"}";

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonStr);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        String txid = null;
        try {
            Pair<String, Integer> res = sendRequest(httpClient, request);
            if (OK_CODE == res.getRight()) {
                JSONObject jsonObject = JSON.parseObject(res.getLeft());
                txid = jsonObject.getString("txid");
            }
        }
        catch (Throwable e) {
            e.printStackTrace();
        }

        return txid;
    }

    public List<UsdtVout> listUsdtBalance() {
        HttpUrl url = HttpUrl.parse(ClientConfig.serverUrl + "/balance/usdt");
        String jsonStr = "{}";

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonStr);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        List<UsdtVout> balances = new ArrayList<>();
        try {
            Pair<String, Integer> res = sendRequest(httpClient, request);
            if (OK_CODE == res.getRight()) {
                JSONObject jsonObject = JSON.parseObject(res.getLeft());
                JSONArray jsonArray = jsonObject.getJSONArray("balance");
                for (Object obj : jsonArray) {
                    JSONObject json = (JSONObject) obj;
                    UsdtVout balance = new UsdtVout();
                    balance.setAddress(json.getString("address"));
                    balance.setAmount(json.getLongValue("amount"));
                    balances.add(balance);
                }
            }
        }
        catch (Throwable e) {
            e.printStackTrace();
        }

        return balances;
    }

    public List<BtcVout> listBtcBalance() {
        HttpUrl url = HttpUrl.parse(ClientConfig.serverUrl + "/balance/btc");
        String jsonStr = "{}";

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonStr);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        List<BtcVout> balances = new ArrayList<>();
        try {
            Pair<String, Integer> res = sendRequest(httpClient, request);
            if (OK_CODE == res.getRight()) {
                JSONObject jsonObject = JSON.parseObject(res.getLeft());
                JSONArray jsonArray = jsonObject.getJSONArray("balance");
                for (Object obj : jsonArray) {
                    JSONObject json = (JSONObject) obj;
                    BtcVout balance = new BtcVout();
                    balance.setAddress(json.getString("address"));
                    balance.setAmount(json.getLongValue("amount"));
                    balances.add(balance);
                }
            }
        }
        catch (Throwable e) {
            e.printStackTrace();
        }

        return balances;
    }

    public List<BtcUtxo> listBtcUtxo() {
        HttpUrl url = HttpUrl.parse(ClientConfig.serverUrl + "/balance/utxo");
        String jsonStr = "{}";

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonStr);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        List<BtcUtxo> balances = new ArrayList<>();
        try {
            Pair<String, Integer> res = sendRequest(httpClient, request);
            if (OK_CODE == res.getRight()) {
                JSONObject jsonObject = JSON.parseObject(res.getLeft());
                JSONArray jsonArray = jsonObject.getJSONArray("utxo");
                for (Object obj : jsonArray) {
                    JSONObject json = (JSONObject) obj;
                    BtcUtxo balance = new BtcUtxo();
                    balance.setTxid(json.getString("txid"));
                    balance.setN(json.getIntValue("n"));
                    balance.setRedeemScript(json.getString("scriptPubKey"));
                    balance.setAddress(json.getString("address"));
                    balance.setAmount(json.getLongValue("amount"));
                    balances.add(balance);
                }
            }
        }
        catch (Throwable e) {
            e.printStackTrace();
        }

        return balances;
    }

}
