package com.funchain.wallet;

public class ClientConfig {

    public static String serverUrl;

    public static int network;

    public static byte chainId;

    public static String usdtFeePubKey;

    public static String btcFeePubKey;

    public static String ethFeePubKey;

    public static String usdtProperty;

}
