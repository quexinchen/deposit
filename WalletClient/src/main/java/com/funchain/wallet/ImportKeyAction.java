package com.funchain.wallet;

import com.funchain.wallet.bip.Encryption;
import com.funchain.wallet.key.KeyCache;
import com.funchain.wallet.key.KeyCacheFactory;
import com.funchain.wallet.key.KeyRecord;
import org.bouncycastle.util.encoders.Hex;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * 导入私钥
 *
 * @author Dev
 */
public class ImportKeyAction {

    public static int importKeys(ImportArg arg) throws Exception {
        KeyCache keyCache = KeyCacheFactory.getKeyCache(arg.getCoinType(), arg.getChangeType());

        BufferedReader reader = null;
        FileReader fin = null;
        fin = new FileReader(arg.getFilePath());
        reader = new BufferedReader(fin);

        int count = 0;
        String str = reader.readLine();
        while (null != str) {
            String[] strs = str.split(",");

            KeyRecord record = new KeyRecord();
            record.setAddressIndex(strs[0].trim());
            record.setPubKey(strs[1].trim());

            String privKey = strs[2].trim();
            byte[] decryptedPrivKey = Encryption.decrypt(arg.getEncodedPassword(), Hex.decode(privKey));
            record.setPrivKey(Hex.toHexString(decryptedPrivKey));

            keyCache.addKeyRecord(record);

            count++;
            str = reader.readLine();
        }
        return count;
    }

}
