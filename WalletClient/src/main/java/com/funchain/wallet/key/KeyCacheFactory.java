package com.funchain.wallet.key;

import java.util.HashMap;
import java.util.Map;

public class KeyCacheFactory {

    private static Map<String, KeyCache> caches = new HashMap<>();

    public static KeyCache getKeyCache(int coinType, int changeType) {
        String cacheId = "" + coinType + "" + changeType;
        KeyCache cache = caches.get(cacheId);
        if (null == cache) {
            caches.put(cacheId, new KeyCache(coinType, changeType));
            cache = caches.get(cacheId);
        }
        return cache;
    }

}
