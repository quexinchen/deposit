package com.funchain.wallet.key;

import lombok.Data;

/**
 * 私钥
 *
 * @author Dev
 */
@Data
public class KeyRecord {

    private String addressIndex;

    private String pubKey;

    private String privKey;

    private String address;

}
