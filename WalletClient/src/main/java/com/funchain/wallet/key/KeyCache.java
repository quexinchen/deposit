package com.funchain.wallet.key;

import com.funchain.wallet.ClientConfig;
import com.funchain.wallet.bip.BTCAddress;
import com.funchain.wallet.bip.ETHAddress;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 缓存私钥
 *
 * @author Dev
 */
public class KeyCache {

    private int coinType;
    private int changeType;

    private Map<String, KeyRecord> keys;

    public KeyCache(int coinType, int changeType) {
        this.coinType = coinType;
        this.changeType = changeType;
        keys = new HashMap<>(100);
    }

    public void addKeyRecord(KeyRecord key) {
        keys.put(key.getPubKey(), key);

        // Address
        try {
            if (coinType == 0 || coinType == 1) {
                String addr = BTCAddress.toP2PKHAddress(ClientConfig.network, key.getPubKey());
                key.setAddress(addr);
                keys.put(addr, key);
            } else if (coinType == 60) {
                String addr = ETHAddress.toAccountAddress(key.getPubKey());
                key.setAddress(addr);
                keys.put(addr, key);
            }
        } catch (Exception e) {
            // ignore
        }
    }

    public KeyRecord getKeyRecord(String pubKey) {
        return keys.get(pubKey);
    }

    public Set<String> keySet() {
        return keys.keySet();
    }

}
