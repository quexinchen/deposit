package com.funchain.wallet.btc;

import lombok.Data;

/**
 * BTC UTXO
 *
 * @author DEV
 */
@Data
public class BtcUtxo {

    private String txid;

    private int n;

    private String pubKey;

    private String redeemScript;

    private String address;

    private long amount;

}
