package com.funchain.wallet.btc;

import com.funchain.wallet.ClientApi;
import com.funchain.wallet.ClientConfig;
import com.funchain.wallet.key.KeyCache;
import com.funchain.wallet.key.KeyCacheFactory;
import com.funchain.wallet.key.KeyRecord;
import com.funchain.wallet.usdt.ForwardArg;

import java.math.BigDecimal;
import java.util.*;

/**
 * BTC自动转账
 *
 * @author Dev
 */
public class BtcAction {

    /**
     * 查询BTC余额列表
     *
     * @return
     */
    public static void balance() {
        List<BtcVout> vouts = ClientApi.getInstance().listBtcBalance();
        System.out.println("BTC addresses: " + vouts.size());
        if (vouts.isEmpty()) {
            return;
        }

        vouts.sort(Comparator.comparing(a -> 0 - a.getAmount()));
        for (BtcVout vout : vouts) {
            StringBuilder sb = new StringBuilder();
            sb.append(vout.getAddress()).append(": ");
            sb.append(new BigDecimal(vout.getAmount()).scaleByPowerOfTen(0 - 8).stripTrailingZeros().toPlainString());
            System.out.println(sb.toString());
        }
    }

    /**
     * 查询UTXO列表
     *
     * @return
     */
    public static void utxo() {
        List<BtcUtxo> utxos = ClientApi.getInstance().listBtcUtxo();
        System.out.println("UTXO: " + utxos.size());
        if (utxos.isEmpty()) {
            return;
        }

        utxos.sort(Comparator.comparing(a -> 0 - a.getAmount()));
        for (BtcUtxo utxo : utxos) {
            StringBuilder sb = new StringBuilder();
            sb.append(utxo.getAddress()).append(": ");
            sb.append(new BigDecimal(utxo.getAmount()).scaleByPowerOfTen(0 - 8).stripTrailingZeros().toPlainString());
            sb.append(" (").append(utxo.getTxid()).append(",").append(utxo.getN()).append(")");
            System.out.println(sb.toString());
        }
    }

    /**
     * 归集BTC余额
     *
     */
    public static int forward(ForwardArg forward) {
        // 查询UTXO列表
        // 过滤余额太小的UTXO
        List<BtcUtxo> tempUtxos = ClientApi.getInstance().listBtcUtxo();
        List<BtcUtxo> utxos = new ArrayList<>();
        for (BtcUtxo utxo : tempUtxos) {
            if (utxo.getAmount() > forward.getMin()) {
                utxos.add(utxo);
            }
        }
        if (utxos.isEmpty()) {
            System.out.println("Empty UTXOs!");
            return 0;
        }

        // 输出到目标地址
        BtcVout receiveVout = new BtcVout();
        receiveVout.setAddress(forward.getDestinationAddr());
        BigDecimal receiveAmount = BigDecimal.ZERO;

        // 找零到固定地址
        // FIXME: 使用固定的BTC地址来支付矿工费
        KeyCache keyCache = KeyCacheFactory.getKeyCache(0, 0);
        KeyRecord feeKey = keyCache.getKeyRecord(ClientConfig.btcFeePubKey);
        BtcVout changeVout = new BtcVout();
        BigDecimal changeAmount = BigDecimal.ZERO;

        // 计算转账金额与找零金额
        List<BtcUtxo> vins = new ArrayList<>();
        for (BtcUtxo utxo : utxos) {
            KeyRecord kr = keyCache.getKeyRecord(utxo.getAddress());
            if (kr == null) {
                continue;
            }

            vins.add(utxo);
            if (kr.getPubKey().equals(feeKey.getPubKey())) {
                changeAmount = changeAmount.add(new BigDecimal(utxo.getAmount()));
                changeVout.setAddress(utxo.getAddress());
            } else {
                receiveAmount = receiveAmount.add(new BigDecimal(utxo.getAmount()));
            }
        }
        if (vins.isEmpty()) {
            System.out.println("Empty UTXOs!");
            return 0;
        }
        receiveVout.setAmount(receiveAmount.longValue());

        // 计算矿工费
        int txSize = utxos.size() * 148 + 2 * 34 + 10;
        long txFee = (long) (0.5 + txSize * forward.getFeeRate());
        changeAmount = changeAmount.subtract(new BigDecimal(txFee));
        if (BigDecimal.ZERO.compareTo(changeAmount) >= 0) {
            System.out.println("Insufficient fee!");
            return 0;
        }
        changeVout.setAmount(changeAmount.longValue());

        // 签名交易
        String rawTx = BtcTxSigner.signTx(ClientConfig.network, utxos, Arrays.asList(receiveVout, changeVout));

        // 广播交易
        String txid = ClientApi.getInstance().broadcastBtcTx(rawTx);
        if (null == txid) {
            System.out.println("Failed to broadcast raw tx:");
            System.out.println(rawTx);
            return 0;
        }
        System.out.println(txid);

        return vins.size();
    }

}
