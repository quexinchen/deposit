package com.funchain.wallet.btc;

import com.funchain.wallet.key.KeyCache;
import com.funchain.wallet.key.KeyCacheFactory;
import org.bitcoinj.core.*;
import org.bitcoinj.crypto.TransactionSignature;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet2Params;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;
import org.bouncycastle.util.encoders.Hex;

import java.util.List;

/**
 * 签名BTC交易
 *
 * @author Dev
 */
public class BtcTxSigner {

    /**
     * Create and sing a raw transaction
     *
     * @param network
     * @param vins
     * @param vouts
     * @return
     */
    public static String signTx(int network, List<BtcUtxo> vins, List<BtcVout> vouts) {
        Transaction tx = createRawTx(network, vins, vouts);
        return signInputs(tx, vins);
    }

    /**
     * Create a raw transaction
     *
     * @param network
     * @return
     */
    public static Transaction createRawTx(int network, List<BtcUtxo> vins, List<BtcVout> vouts) {
        NetworkParameters networkParam = (0 == network) ? MainNetParams.get() : TestNet2Params.get();
        Transaction transaction = new Transaction(networkParam);

        for (BtcUtxo utxo : vins) {
            TransactionOutPoint outPoint = new TransactionOutPoint(networkParam,
                    utxo.getN(), Sha256Hash.wrap(Utils.HEX.decode(utxo.getTxid())));
            TransactionInput vin = new TransactionInput(networkParam, transaction, new byte[0], outPoint);
            transaction.addInput(vin);
        }

        for (BtcVout vout : vouts) {
            transaction.addOutput(Coin.valueOf(vout.getAmount()), Address.fromBase58(networkParam, vout.getAddress()));
        }

        return transaction;
    }

    /**
     * Sign the transaction inputs
     *
     * @param transaction
     * @param vins
     * @return
     */
    public static String signInputs(Transaction transaction, List<BtcUtxo> vins) {
        KeyCache keyCache = KeyCacheFactory.getKeyCache(0, 0);

        int k = 0;
        for (BtcUtxo utxo : vins) {
            // 被签名的哈希值
            Sha256Hash hash = transaction.hashForSignature(k,
                    Utils.HEX.decode(utxo.getRedeemScript()), Transaction.SigHash.ALL, false);

            // 用私钥签名
            String privKey = keyCache.getKeyRecord(utxo.getAddress()).getPrivKey();
            ECKey ecKey = ECKey.fromPrivate(Hex.decode(privKey));
            ECKey.ECDSASignature signature = ecKey.sign(hash);

            // 生成签名脚本
            TransactionSignature txSig = new TransactionSignature(signature.r, signature.s);
            Script script = ScriptBuilder.createInputScript(txSig, ecKey);
            transaction.getInput(k).setScriptSig(script);

            k++;
        }

        return Utils.HEX.encode(transaction.bitcoinSerialize());
    }

}
