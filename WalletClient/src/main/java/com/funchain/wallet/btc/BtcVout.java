package com.funchain.wallet.btc;

import lombok.Data;

/**
 * BTC输出
 *
 * @author Dev
 */
@Data
public class BtcVout {

    private String address;

    private long amount;

}
