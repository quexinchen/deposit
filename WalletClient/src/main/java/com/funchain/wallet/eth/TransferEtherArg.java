package com.funchain.wallet.eth;

import lombok.Data;

/**
 * 转账ETH的命令参数
 *
 * @author Dev
 */
@Data
public class TransferEtherArg {

    private String from;

    private String to;

    private long amount;

    private long nonce;

    private long gasLimit;

    private long gasPrice;

}
