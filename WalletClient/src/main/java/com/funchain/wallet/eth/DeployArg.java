package com.funchain.wallet.eth;

import lombok.Data;

/**
 * 部署ETH合约
 *
 * @author Dev
 */
@Data
public class DeployArg {

    private String filePath;

    private String destinationAddress;

    private long nonce;

    private long gasPrice;

    private long gasLimit;

    private int number;

    private String pubKey;

}
