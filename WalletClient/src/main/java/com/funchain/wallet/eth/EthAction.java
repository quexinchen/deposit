package com.funchain.wallet.eth;

import com.funchain.wallet.ClientApi;
import com.funchain.wallet.ClientConfig;
import com.funchain.wallet.key.KeyCache;
import com.funchain.wallet.key.KeyCacheFactory;
import com.funchain.wallet.key.KeyRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * ETH操作
 *
 * @author Dev
 */
public class EthAction {

    /**
     * Deploy contracts
     *
     * @param arg
     * @return
     * @throws Exception
     */
    public static int deploy(DeployArg arg) throws Exception {
        // check private key
        KeyCache keyCache = KeyCacheFactory.getKeyCache(60, 0);
        Set<String> keySet = keyCache.keySet();
        if (keySet.isEmpty()) {
            throw new Exception("No ETH private key!");
        }
        String pubKey = ClientConfig.ethFeePubKey;

        // load contract
        File file = new File(arg.getFilePath());
        String contract = FileUtils.readFileToString(file, Charset.defaultCharset());

        // set parameter
        contract = contract + arg.getDestinationAddress().substring(2);

        // chain
        byte chainId = ClientConfig.chainId;

        // sign tx
        List<String> txidList = new ArrayList<>();
        int count = 0;
        for (int i = 0; i < arg.getNumber(); i++) {
            String rawTx = EthTxSigner.createContract(chainId, pubKey,
                    arg.getNonce() + i,
                    arg.getGasPrice() * 1000_000_000L,
                    arg.getGasLimit(),
                    contract);

            // broadcast tx
            String txid = ClientApi.getInstance().broadcastEthTx(rawTx);
            txidList.add(txid);
            if (null != txid) {
                count++;
            }
        }

        // log txids
        log(txidList);

        return count;
    }

    private static void log(List<String> txidList) throws Exception {
        String orderId = "" + System.currentTimeMillis();

        String txidFile = "txid." + orderId + ".csv";
        BufferedWriter txidWriter = new BufferedWriter(new FileWriter(txidFile));

        for (String txid : txidList) {
            IOUtils.write(txid + IOUtils.LINE_SEPARATOR, txidWriter);
        }

        txidWriter.flush();
        txidWriter.close();

        System.out.println(txidFile);
    }

    /**
     * 转账ETH
     */
    public static void transferEther(TransferEtherArg arg) throws Exception {
        // retrieve private key
        KeyCache keyCache = KeyCacheFactory.getKeyCache(60, 0);
        Set<String> keySet = keyCache.keySet();
        if (keySet.isEmpty()) {
            throw new Exception("No ETH private key!");
        }
        KeyRecord key = keyCache.getKeyRecord(arg.getFrom());
        if (null == key) {
            throw new Exception("No ETH private key!");
        }

        // sign tx
        String rawTx = EthTxSigner.transferEther(ClientConfig.chainId, key.getPrivKey(),
                arg.getNonce(), arg.getGasPrice(), arg.getGasLimit(), arg.getTo(), arg.getAmount());

        // broadcast
        try {
            String txid = ClientApi.getInstance().broadcastEthTx(rawTx);
            System.out.println(txid);
        } catch (Exception e) {
            System.out.println(rawTx);
            e.printStackTrace();
        }
    }

}
