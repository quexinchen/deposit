package com.funchain.wallet.eth;

import com.funchain.wallet.key.KeyCache;
import com.funchain.wallet.key.KeyCacheFactory;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.utils.Numeric;

import java.math.BigInteger;

/**
 * 签名ETH交易
 *
 * @author Dev
 */
public class EthTxSigner {

    /**
     * 创建合约
     *
     * @param chainId
     * @param pubKey
     * @param _nonce
     * @param _gasPrice
     * @param _gasLimit
     * @param data
     * @return
     */
    public static String createContract(byte chainId, String pubKey,
            long _nonce, long _gasPrice, long _gasLimit, String data) {
        KeyCache keyCache = KeyCacheFactory.getKeyCache(60, 0);
        String privKey = keyCache.getKeyRecord(pubKey).getPrivKey();
        Credentials credentials = Credentials.create(privKey);

        BigInteger nonce = BigInteger.valueOf(_nonce);
        BigInteger gasPrice = BigInteger.valueOf(_gasPrice);
        BigInteger gasLimit = BigInteger.valueOf(_gasLimit);

        RawTransaction transaction = RawTransaction.createContractTransaction(nonce, gasPrice, gasLimit, BigInteger.ZERO, data);

        byte[] rawTx = TransactionEncoder.signMessage(transaction, chainId, credentials);
        return Numeric.toHexString(rawTx);
    }

    /**
     * 转账ETH
     *
     * @param chainId
     * @param privKey
     * @param _nonce
     * @param _gasPrice
     * @param _gasLimit
     * @param to
     * @param _value
     * @return
     */
    public static String transferEther(byte chainId, String privKey,
                                       long _nonce, long _gasPrice, long _gasLimit, String to, long _value) {
        BigInteger nonce = BigInteger.valueOf(_nonce);
        BigInteger gasPrice = BigInteger.valueOf(_gasPrice);
        BigInteger gasLimit = BigInteger.valueOf(_gasLimit);
        BigInteger value = BigInteger.valueOf(_value);

        Credentials credentials = Credentials.create(privKey);

        RawTransaction transaction = RawTransaction.createEtherTransaction(nonce, gasPrice, gasLimit, to, value);

        byte[] rawTx = TransactionEncoder.signMessage(transaction, chainId, credentials);
        return Numeric.toHexString(rawTx);
    }

}
