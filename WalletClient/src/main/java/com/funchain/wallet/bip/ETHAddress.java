package com.funchain.wallet.bip;

import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.Utils;
import org.bitcoinj.crypto.DeterministicKey;
import org.web3j.crypto.Keys;
import org.web3j.utils.Numeric;

import java.util.Arrays;

/**
 * 生成ETH地址
 *
 * @author Dev
 */
public class ETHAddress {

    public static String toAccountAddress(String pubKey) {
        ECKey key = DeterministicKey.fromPublicOnly(Utils.HEX.decode(pubKey));

        byte[] uncompressedPub  = key.getPubKeyPoint().getEncoded(false);
        byte[] result = Keys.getAddress(Arrays.copyOfRange(uncompressedPub, 1, uncompressedPub.length));
        return Numeric.toHexString(result);
    }

}
