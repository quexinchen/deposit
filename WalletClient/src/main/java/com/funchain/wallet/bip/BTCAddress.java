package com.funchain.wallet.bip;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Utils;
import org.bitcoinj.crypto.DeterministicKey;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * 生成BTC地址
 *
 * @author Dev
 */
public class BTCAddress {

    public static String toP2PKHAddress(int network, String pubKey) {
        ECKey key = DeterministicKey.fromPublicOnly(Utils.HEX.decode(pubKey));

        NetworkParameters parameters = network == 0 ? MainNetParams.get() : TestNet3Params.get();
        return key.toAddress(parameters).toBase58();
    }

    public static String toMultiSigAddress(int network, int m, List<String> pubKeys) {
        NetworkParameters parameters = network == 0 ? MainNetParams.get() : TestNet3Params.get();

        List<ECKey> keyList = new ArrayList<>();
        for (String pubKey : pubKeys) {
            ECKey ecKey = ECKey.fromPublicOnly(Utils.HEX.decode(pubKey));
            keyList.add(ecKey);
        }
        keyList.sort(Comparator.comparing(ECKey::getPublicKeyAsHex));

        Script script = ScriptBuilder.createMultiSigOutputScript(m, keyList);
        byte[] hash160 = Utils.sha256hash160(script.getProgram());
        return Address.fromP2SHHash(parameters, hash160).toString();
    }

    public static String toMultiSigScript(int network, int m, List<String> pubKeys) {
        List<ECKey> keyList = new ArrayList<>();
        for (String pubKey : pubKeys) {
            ECKey ecKey = ECKey.fromPublicOnly(Utils.HEX.decode(pubKey));
            keyList.add(ecKey);
        }
        keyList.sort(Comparator.comparing(ECKey::getPublicKeyAsHex));

        Script script = ScriptBuilder.createMultiSigOutputScript(m, keyList);
        String redeemScript = Utils.HEX.encode(script.getProgram());

        byte[] hash160 = Utils.sha256hash160(script.getProgram());
        String scriptPubKey = Utils.HEX.encode(hash160);

        return redeemScript;
    }

}
