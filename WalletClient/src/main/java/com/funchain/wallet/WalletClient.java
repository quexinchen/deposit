package com.funchain.wallet;

import com.beust.jcommander.JCommander;
import com.funchain.wallet.bip.Encryption;
import com.funchain.wallet.btc.BtcAction;
import com.funchain.wallet.eth.DeployArg;
import com.funchain.wallet.eth.EthAction;
import com.funchain.wallet.eth.TransferEtherArg;
import com.funchain.wallet.usdt.ForwardArg;
import com.funchain.wallet.usdt.UsdtAction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class WalletClient {

    private void importKeys(Scanner in, String[] parameters) {
        if (parameters == null || parameters.length != 2) {
            System.out.println("import needs 2 parameters like the following: ");
            System.out.println("import 44'/0'/0'/0 export0.csv");
            System.out.println("import 44'/60'/0'/0 export60.csv");
            System.out.println(" ");
            return;
        }

        ImportArg importArg = new ImportArg();

        String pathStr = parameters[0];
        try {
            String[] strs = pathStr.split("/");
            if (strs.length < 2) {
                System.out.println("Wrong path!");
                return;
            }

            String aStr = strs[1];
            if (!aStr.endsWith("'")) {
                System.out.println("Wrong path!");
                return;
            }
            int coinType = Integer.parseInt(aStr.substring(0, aStr.length() - 1));
            importArg.setCoinType(coinType);

            importArg.setChangeType(0);
            if (strs.length >= 4) {
                aStr = strs[3];
                int changeType = Integer.parseInt(aStr);
                importArg.setChangeType(changeType);
            }
        } catch (Exception e) {
            System.out.println("Wrong path!");
            return;
        }

        try {
            String filePath = parameters[1].trim();
            importArg.setFilePath(filePath);
        } catch (Exception e) {
            System.out.println("Wrong file path!");
            return;
        }

        // password
        try {
            char[] password = PasswordUtils.inputPassword();

            byte[] encodePassword = Encryption.encodePassword(PasswordUtils.char2Byte(password));
            importArg.setEncodedPassword(encodePassword);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        try {
            int count = ImportKeyAction.importKeys(importArg);
            System.out.println("Imported " + count + " keys");
        } catch (Exception e) {
            System.out.println("Failed to import the keys!");
            e.printStackTrace();
        }
        System.out.println(" ");
    }

    private void deploy(String[] parameters) {
        if (parameters == null || parameters.length != 6) {
            System.out.println("deploy needs 6 parameters like the following: ");
            System.out.println("deploy ForwarderContract 0x4a9676e2f51e0e4d5113956d436815627d619901 700000 5 0 10");
            System.out.println(" ");
            return;
        }

        DeployArg arg = new DeployArg();

        try {
            arg.setFilePath(parameters[0]);
            arg.setDestinationAddress(parameters[1]);

            arg.setGasLimit(Long.parseLong(parameters[2]));
            arg.setGasPrice(Long.parseLong(parameters[3]));
            arg.setNonce(Long.parseLong(parameters[4]));

            arg.setNumber(Integer.parseInt(parameters[5]));

            int count = EthAction.deploy(arg);
            System.out.println("Deployed " + count + " contracts");
        } catch (Exception e) {
            System.out.println("Failed to deploy the contract!");
            e.printStackTrace();
        }
        System.out.println(" ");
    }

    private void list(String[] parameters) {
        if (parameters == null || parameters.length != 1) {
            System.out.println("list needs 1 parameters like the following: ");
            System.out.println("list usdt");
            System.out.println("list btc");
            System.out.println("list utxo");
            System.out.println(" ");
            return;
        }

        try {
            String type = parameters[0];
            if ("usdt".equalsIgnoreCase(type)) {
                UsdtAction.balance();
            } else if ("btc".equalsIgnoreCase(type)) {
                BtcAction.balance();
            } else if ("utxo".equalsIgnoreCase(type)) {
                BtcAction.utxo();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(" ");
    }

    private void cusdt(String[] parameters) {
        if (parameters == null || parameters.length != 3) {
            System.out.println("cusdt needs 3 parameters like the following: ");
            System.out.println("cusdt mmbfaT8ZyDiij7nT1SnNGzNFsr3z58XvrH 100 1.0");
            System.out.println(" ");
            return;
        }

        ForwardArg arg = new ForwardArg();

        try {
            arg.setDestinationAddr(parameters[0]);

            arg.setMin(Long.parseLong(parameters[1]) * 10000_0000L);
            arg.setFeeRate(Double.parseDouble(parameters[2]));

            int count = UsdtAction.forward(arg);
            System.out.println("Collected " + count + " USDT addresses");
        } catch (Exception e) {
            System.out.println("Failed to collect the USDT!");
            e.printStackTrace();
        }
        System.out.println(" ");
    }

    private void cbtc(String[] parameters) {
        if (parameters == null || parameters.length != 3) {
            System.out.println("cbtc needs 3 parameters like the following: ");
            System.out.println("cbtc mmbfaT8ZyDiij7nT1SnNGzNFsr3z58XvrH 0.001 1.5");
            System.out.println(" ");
            return;
        }

        ForwardArg arg = new ForwardArg();

        try {
            arg.setDestinationAddr(parameters[0]);

            arg.setMin((long)(0.5 + Double.parseDouble(parameters[1]) * 10000_0000));
            arg.setFeeRate(Double.parseDouble(parameters[2]));

            int count = BtcAction.forward(arg);
            System.out.println("Collected " + count + " BTC UTXOs");
        } catch (Exception e) {
            System.out.println("Failed to collect the BTC!");
            e.printStackTrace();
        }
        System.out.println(" ");
    }

    /**
     * ETH转账
     */
    private void transferEther(String[] parameters) {
        if (parameters == null || parameters.length != 6) {
            System.out.println("ethTransfer needs 6 parameters like the following: ");
            System.out.println("ethTransfer 0x35b571d1e0bf3c469e85518742813cde8c9c2ca4 2 21000 82 0x56ddc11f890997af41892a8f35eb36e7b770003f 0.5");
            System.out.println(" ");
            return;
        }

        TransferEtherArg arg = new TransferEtherArg();
        try {
            arg.setFrom(parameters[0]);

            arg.setGasPrice(Long.parseLong(parameters[1]) * 10_0000_0000L);
            arg.setGasLimit(Long.parseLong(parameters[2]));

            arg.setNonce(Long.parseLong(parameters[3]));

            arg.setTo(parameters[4]);

            BigDecimal value = new BigDecimal(parameters[5]);
            arg.setAmount(value.scaleByPowerOfTen(18).setScale(0, RoundingMode.HALF_UP).longValue());

            EthAction.transferEther(arg);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(" ");
            return;
        }

        System.out.println(" ");
    }

    private void help() {
        System.out.println("Help: List of Wallet Client commands");
        System.out.println("import");
        System.out.println("deploy");
        System.out.println("list");
        System.out.println("cusdt");
        System.out.println("cbtc");
        System.out.println("quit");
        System.out.println("Input any one of the listed commands, to display how-to tips.");
        System.out.println(" ");
    }

    private String[] getCmd(String cmdLine) {
        if (cmdLine.isEmpty()) {
            return cmdLine.split("\\s+");
        }
        String[] strArray = cmdLine.split("\"");
        int num = strArray.length;
        int start = 0;
        int end = 0;
        if (cmdLine.charAt(0) == '\"') {
            start = 1;
        }
        if (cmdLine.charAt(cmdLine.length() - 1) == '\"') {
            end = 1;
        }
        if (((num + end) & 1) == 0) {
            return new String[]{"ErrorInput"};
        }

        List<String> cmdList = new ArrayList<>();
        for (int i = start; i < strArray.length; i++) {
            if ((i & 1) == 0) {
                cmdList.addAll(Arrays.asList(strArray[i].trim().split("\\s+")));
            } else {
                cmdList.add(strArray[i].trim());
            }
        }
        Iterator ito = cmdList.iterator();
        while (ito.hasNext()) {
            if (ito.next().equals("")) {
                ito.remove();
            }
        }
        String[] result = new String[cmdList.size()];
        return cmdList.toArray(result);
    }

    private void run() {
        Scanner in = new Scanner(System.in);

        this.init();

        while (in.hasNextLine()) {
            String cmd = "";
            try {
                String cmdLine = in.nextLine().trim();
                String[] cmdArray = getCmd(cmdLine);
                // split on trim() string will always return at the minimum: [""]
                cmd = cmdArray[0];
                if ("".equals(cmd)) {
                    continue;
                }
                String[] parameters = Arrays.copyOfRange(cmdArray, 1, cmdArray.length);
                String cmdLowerCase = cmd.toLowerCase();

                switch (cmdLowerCase) {
                    case "help": {
                        help();
                        break;
                    }
                    case "import": {
                        importKeys(in, parameters);
                        break;
                    }
                    case "deploy": {
                        deploy(parameters);
                        break;
                    }
                    case "list": {
                        list(parameters);
                        break;
                    }
                    case "cusdt": {
                        cusdt(parameters);
                        break;
                    }
                    case "cbtc": {
                        cbtc(parameters);
                        break;
                    }
                    case "ethtransfer": {
                        transferEther(parameters);
                        break;
                    }
                    case "exit":
                    case "quit": {
                        System.out.println("Exit !!!");
                        return;
                    }
                    default: {
                        System.out.println("Invalid cmd: " + cmd);
                        help();
                    }
                }
            } catch (Exception e) {
                System.out.println(cmd + " failed!");
                e.printStackTrace();
            }
        }
    }

    private void init() {
        System.out.println(" ");
        System.out.println("Welcome to Wallet Client");
        System.out.println(" ");

        // help
        help();
    }

    public static void main(String[] args) {
        WalletClient cli = new WalletClient();

        cli.loadConfig();
        System.out.println("Wallet Server: " + ClientConfig.serverUrl);

        JCommander.newBuilder()
                .addObject(cli)
                .build()
                .parse(args);

        cli.run();
    }

    private void loadConfig() {
        try {
            FileReader fin = new FileReader("client.config");
            BufferedReader reader = new BufferedReader(fin);
            Properties properties = new Properties();
            properties.load(reader);

            ClientConfig.serverUrl = properties.getProperty("server-url");

            String network = properties.getProperty("btc-network");
            ClientConfig.network = Integer.parseInt(network);

            String chainId = properties.getProperty("eth-chainid");
            ClientConfig.chainId = Byte.parseByte(chainId);

            ClientConfig.usdtFeePubKey = properties.getProperty("usdt-fee-pubkey");
            ClientConfig.btcFeePubKey = properties.getProperty("btc-fee-pubkey");
            ClientConfig.ethFeePubKey = properties.getProperty("eth-fee-pubkey");

            ClientConfig.usdtProperty = properties.getProperty("usdt-property");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

}
