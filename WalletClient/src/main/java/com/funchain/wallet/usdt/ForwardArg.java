package com.funchain.wallet.usdt;

import lombok.Data;

/**
 * 归集参数
 *
 * @author Dev
 */
@Data
public class ForwardArg {

    private String destinationAddr;

    private long min;

    private double feeRate;

}
