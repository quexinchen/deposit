package com.funchain.wallet.usdt;

import com.funchain.wallet.ClientApi;
import com.funchain.wallet.ClientConfig;
import com.funchain.wallet.btc.BtcUtxo;
import com.funchain.wallet.btc.BtcVout;
import com.funchain.wallet.key.KeyCache;
import com.funchain.wallet.key.KeyCacheFactory;
import com.funchain.wallet.key.KeyRecord;

import java.math.BigDecimal;
import java.util.*;

/**
 * USDT操作
 *
 * @author Dev
 */
public class UsdtAction {

    public static long dust = 546L;

    public static void balance() {
        List<UsdtVout> vouts = ClientApi.getInstance().listUsdtBalance();
        System.out.println("USDT addresses: " + vouts.size());
        if (vouts.isEmpty()) {
            return;
        }

        vouts.sort(Comparator.comparing(a -> 0 - a.getAmount()));
        for (UsdtVout vout : vouts) {
            StringBuilder sb = new StringBuilder();
            sb.append(vout.getAddress()).append(": ");
            sb.append(new BigDecimal(vout.getAmount()).scaleByPowerOfTen(0 - 8).stripTrailingZeros().toPlainString());
            System.out.println(sb.toString());
        }
    }

    public static int forward(ForwardArg forward) {
        // 查询USDT余额列表
        // 过滤余额太小的地址
        List<UsdtVout> usdtVouts = ClientApi.getInstance().listUsdtBalance();
        Map<String, Long> usdtMap = new HashMap<>();
        for (UsdtVout vout : usdtVouts) {
            if (vout.getAmount() > forward.getMin()) {
                usdtMap.put(vout.getAddress(), vout.getAmount());
            }
        }
        if (usdtMap.isEmpty()) {
            return 0;
        }

        // 查询UTXO列表
        // 给每个USDT地址找到一个BTC为546聪的UTXO
        List<BtcUtxo> utxos = ClientApi.getInstance().listBtcUtxo();
        Map<String, BtcUtxo> vinMap = new HashMap<>();
        for (BtcUtxo utxo : utxos) {
            if (utxo.getAmount() != dust) {
                continue;
            }
            if (!usdtMap.containsKey(utxo.getAddress())) {
                continue;
            }
            if (vinMap.containsKey(utxo.getAddress())) {
                continue;
            }

            utxo.setAmount(usdtMap.get(utxo.getAddress()));
            vinMap.put(utxo.getAddress(), utxo);
        }
        if (vinMap.isEmpty()) {
            return 0;
        }

        // FIXME: 使用固定的BTC地址来支付矿工费
        KeyCache keyCache = KeyCacheFactory.getKeyCache(0, 0);
        KeyRecord feeKey = keyCache.getKeyRecord(ClientConfig.usdtFeePubKey);
        BtcUtxo feeUtxo = null;
        for (BtcUtxo utxo : utxos) {
            KeyRecord kr = keyCache.getKeyRecord(utxo.getAddress());
            if (kr == null) {
                continue;
            }
            if (kr.getPubKey().equals(feeKey.getPubKey())) {
                feeUtxo = utxo;
                break;
            }
        }

        // 余额从大从小排序
        List<BtcUtxo> records = new ArrayList<>(vinMap.values());
        records.sort(Comparator.comparing(a -> 0 - a.getAmount()));

        // 计算矿工费
        int txSize = 2 * 148 + 2 * 34 + 30 + 10;
        int txFee = (int)(0.5 + txSize * forward.getFeeRate());

        // 逐个地址处理
        int count = 0;
        int network = ClientConfig.network;
        String prevTxid = feeUtxo.getTxid();
        int prevN = feeUtxo.getN();
        long prevAmount = feeUtxo.getAmount();
        for (BtcUtxo addr : records) {
            prevAmount = prevAmount - txFee;

            BtcUtxo vin1 = new BtcUtxo();
            vin1.setTxid(prevTxid);
            vin1.setN(prevN);
            vin1.setAddress(feeUtxo.getAddress());
            vin1.setRedeemScript(feeUtxo.getRedeemScript());

            List<BtcUtxo> vins = new ArrayList<>();
            vins.add(addr);
            vins.add(vin1);

            UsdtVout uVout = new UsdtVout();
            uVout.setAddress(forward.getDestinationAddr());
            uVout.setAmount(addr.getAmount());
            uVout.setDust(dust);

            BtcVout bVout = new BtcVout();
            bVout.setAddress(feeUtxo.getAddress());
            bVout.setAmount(prevAmount);

            // 签名交易
            String rawTx = UsdtTxSigner.signTx(network, vins, uVout, bVout);

            // 广播交易
            String txid = ClientApi.getInstance().broadcastBtcTx(rawTx);
            if (null == txid) {
                System.out.println("Failed to broadcast raw tx:");
                System.out.println(rawTx);
                break;
            }
            System.out.println(txid);

            prevTxid = txid;
            prevN = 0;
            count++;
        }

        return count;
    }

}
