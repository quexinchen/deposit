package com.funchain.wallet.usdt;

import com.funchain.wallet.btc.BtcUtxo;
import lombok.Data;

/**
 * USDT输出
 *
 * @author Dev
 */
@Data
public class UsdtVout {

    private String address;

    private long amount;

    private long dust;

}
