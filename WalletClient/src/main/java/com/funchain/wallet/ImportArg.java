package com.funchain.wallet;

import lombok.Data;

/**
 *
 * @author Dev
 */
@Data
public class ImportArg {

    private int coinType;

    private int changeType;

    private String filePath;

    private byte[] encodedPassword;

}
