package com.funchain.wallet;

import com.funchain.wallet.model.BalanceElement;
import com.funchain.wallet.model.UtxoElement;
import com.funchain.wallet.service.OpenService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
public class OpenServiceTest {

    private OpenService openService;

    @Before
    public void setUp() {
        openService = new OpenService();
        openService.init();
    }

    @Test
    public void testLoginProd() {
        //String webUrl = "http://47.96.106.4:8890";
        String webUrl = "https://api.fingoapp.com";
        openService.clientId = "d50a8edd0a6644f88d37b57539d06ad6";
        openService.clientCode = "f5eecf068a565a33d2b7ca28517fdb653f93cb94";
        String accessToken = openService.login(webUrl);
        System.out.println(accessToken);
    }

    @Test
    public void testLogin() {
        String webUrl = "http://116.62.61.38:8890";
        String accessToken = openService.login(webUrl);
        System.out.println(accessToken);
    }

    @Test
    public void testTokenBalance() {
        String webUrl = "http://116.62.61.38:8890";
        String accessToken = "db027092-f363-4f27-9105-6904f4860060";
        List<BalanceElement> list = openService.listTokenBalance(webUrl, accessToken);
        System.out.println(list);
    }

    @Test
    public void testCoinBalance() {
        String webUrl = "http://116.62.61.38:8890";
        String accessToken = "db027092-f363-4f27-9105-6904f4860060";
        List<BalanceElement> list = openService.listCoinBalance(webUrl, accessToken);
        System.out.println(list);
    }

    @Test
    public void testUtxo() {
        String webUrl = "http://116.62.61.38:8890";
        String accessToken = "db027092-f363-4f27-9105-6904f4860060";
        List<UtxoElement> list = openService.listUtxo(webUrl, accessToken);
        System.out.println(list);
    }

}
