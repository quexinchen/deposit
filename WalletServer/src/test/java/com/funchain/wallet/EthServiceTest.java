package com.funchain.wallet;

import com.funchain.wallet.service.EthService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class EthServiceTest {

    private EthService ethService;

    @Before
    public void setUp() {
        ethService = new EthService();
        ethService.ethNodeUrl = "http://59.110.125.182:8181";
        ethService.init();
    }

    @Test
    public void testContractAddress() {
        String txid = "0x17d441c1dbddb8cce2e4e6d8807d54a74881200e8489aea61fa5a34a73ee5ed4";
        String contractAddr = ethService.getContractAddressByTx(txid);
        System.out.println(contractAddr);
    }

    @Test
    public void testContractAddressSQL() throws Exception {
        int idx = 11;
        String filePath = "txid.csv";
        List<String> txidList = importTxids(filePath);

        for (String txid : txidList) {
            String addr = ethService.getContractAddressByTx(txid);

            StringBuilder sb = new StringBuilder();
            sb.append("insert into p_deposit_address(wallet_id,coin_type,address_index,address) ");
            sb.append("values(2147483641,60,").append(idx).append("'");
            sb.append(addr);
            sb.append("');");
            System.out.println(sb.toString());
            idx++;
        }
    }

    public List<String> importTxids(String filePath) throws Exception {
        BufferedReader reader = null;
        FileReader fin = null;
        fin = new FileReader(filePath);
        reader = new BufferedReader(fin);

        List<String> records = new ArrayList<>();

        String str = reader.readLine();
        while (null != str) {
            records.add(str.trim());

            str = reader.readLine();
        }
        return records;
    }

}
