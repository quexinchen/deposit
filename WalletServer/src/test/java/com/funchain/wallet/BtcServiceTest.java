package com.funchain.wallet;

import com.funchain.wallet.service.BtcService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class BtcServiceTest {

    private BtcService btcService;

    @Before
    public void setUp() throws Exception {
        btcService = new BtcService();
        btcService.btcNodeUrl = "http://47.106.196.27:4001/insight-api";
        btcService.init();
    }

    @Test
    public void testBroadcast() throws Exception {
        String rawTx = "01000000021b5ceee74634d08a602783260edee8d084b897b1a4d435b5fdc4bf2449d141c7000000006b483045022100df7512a716356457d911b4074ca95bb75d6f2efc8ce7c39cad4317bca081c0b5022067578a0cc2924dc01134851f0da31606287956506b4994f82c56494e3ecba5cf0121032f0c2b033bcf6e7002f75c80f98ff08ae0eb6f836fa7cd8a5e462703fb03e56cffffffffccec75821e227ec7f6b69efef954b0b1fb12c997cb7cec9d22c857ff259e8ea5020000006a47304402204158d48a0d8489861a7a867d47a32e337a75ed03186ac03f116ca05f61f24dc1022040fa1c0e58b66e244dc982f56191abbd485641c1be56c8fdce762d33df8fa849012102cb4c5e795e5d408247f5bcac6ad3a29baaf7176ee713ded9bcacc1f630ea48f1ffffffff0398929800000000001976a914f2b9a15e884c678b830b7f3171f3986b64d183d688ac0000000000000000166a146f6d6e6900000000800004e100000002540be40022020000000000001976a914213d8c2c0df33f13ad01c7d2e0bdbb628d7939a188ac00000000";
        String txid = btcService.broadcast(rawTx);
        System.out.println(txid);
    }

}
