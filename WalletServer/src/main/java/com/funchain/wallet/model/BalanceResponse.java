package com.funchain.wallet.model;

import lombok.Data;

/**
 * 余额
 *
 * @author Dev
 */
@Data
public class BalanceResponse {

    private BalanceElement[] balance;

}
