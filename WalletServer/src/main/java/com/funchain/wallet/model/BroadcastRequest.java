package com.funchain.wallet.model;

import lombok.Data;

/**
 * 广播交易
 *
 * @author Dev
 */
@Data
public class BroadcastRequest {

    private String rawTx;

}
