package com.funchain.wallet.model;

import lombok.Data;

/**
 * UTXO
 *
 * @author Dev
 */
@Data
public class UtxoElement {

    private String txid;
    private int n;
    private String address;
    private String scriptPubKey;
    private long amount;

}
