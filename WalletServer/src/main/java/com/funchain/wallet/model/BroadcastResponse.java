package com.funchain.wallet.model;

import lombok.Data;

/**
 * 广播交易
 *
 * @author Dev
 */
@Data
public class BroadcastResponse {

    private String txid;

    private String error;

}
