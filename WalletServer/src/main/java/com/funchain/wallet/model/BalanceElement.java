package com.funchain.wallet.model;

import lombok.Data;

/**
 *
 * @author Dev
 */
@Data
public class BalanceElement {

    private String address;

    private long amount;

}
