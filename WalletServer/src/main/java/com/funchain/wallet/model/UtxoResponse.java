package com.funchain.wallet.model;

import lombok.Data;

/**
 * UTXO
 *
 * @author Dev
 */
@Data
public class UtxoResponse {

    private UtxoElement[] utxo;

}
