package com.funchain.wallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.funchain.wallet")
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class WalletServer {

    public static void main(String[] args) {
        SpringApplication.run(WalletServer.class, args);
    }

}
