package com.funchain.wallet.controller;

import com.funchain.wallet.model.BalanceElement;
import com.funchain.wallet.model.BalanceResponse;
import com.funchain.wallet.model.UtxoElement;
import com.funchain.wallet.model.UtxoResponse;
import com.funchain.wallet.service.OpenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 查询余额
 *
 * @author Dev
 */
@RestController("blncCtrl")
@RequestMapping("/balance")
@Slf4j
public class BalanceController {

    @Autowired
    private OpenService openService;

    /**
     * 查询USDT余额
     *
     * @return
     */
    @PostMapping("/usdt")
    public BalanceResponse usdtBalance() {
        log.info("usdt balance entrance");
        BalanceResponse response = new BalanceResponse();

        List<BalanceElement> results = openService.listTokenBalance();
        response.setBalance(results.toArray(new BalanceElement[0]));

        log.info("usdt balance exit");
        return response;
    }

    /**
     * 查询BTC余额
     *
     * @return
     */
    @PostMapping("/btc")
    public BalanceResponse btcBalance() {
        log.info("btc balance entrance");
        BalanceResponse response = new BalanceResponse();

        List<BalanceElement> results = openService.listCoinBalance();
        response.setBalance(results.toArray(new BalanceElement[0]));

        log.info("btc balance exit");
        return response;
    }

    /**
     * 查询UTXO
     *
     * @return
     */
    @PostMapping("/utxo")
    public UtxoResponse utxo() {
        log.info("utxo entrance");
        UtxoResponse  response = new UtxoResponse();

        List<UtxoElement> results = openService.listUtxo();
        response.setUtxo(results.toArray(new UtxoElement[0]));

        log.info("utxo exit");
        return response;
    }

}
