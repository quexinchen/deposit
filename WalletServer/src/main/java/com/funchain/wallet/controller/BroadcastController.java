package com.funchain.wallet.controller;

import com.funchain.wallet.model.BroadcastRequest;
import com.funchain.wallet.model.BroadcastResponse;
import com.funchain.wallet.service.BtcService;
import com.funchain.wallet.service.EthService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 广播交易
 *
 * @author Dev
 */
@RestController("brdcstCtrl")
@RequestMapping("/broadcast")
@Slf4j
public class BroadcastController {

    @Autowired
    private EthService ethService;

    @Autowired
    private BtcService btcService;

    /**
     * 广播ETH交易
     *
     * @param request
     * @return
     */
    @PostMapping("/eth")
    public BroadcastResponse sendEth(@RequestBody BroadcastRequest request) {
        log.info("enter send ETH: {}", request);

        BroadcastResponse response = new BroadcastResponse();

        String rawTx = StringUtils.trimToNull(request.getRawTx());
        if (null == rawTx) {
            response.setError("Null raw tx");
        }

        try {
            String txid = ethService.broadcast(rawTx);
            response.setTxid(txid);
        } catch (Exception e) {
            e.printStackTrace();
            response.setError(e.getMessage());
        }

        log.info("exit send ETH: {}", response);
        return response;
    }

    /**
     * 广播BTC交易
     *
     * @param request
     * @return
     */
    @PostMapping("/btc")
    public BroadcastResponse sendBtc(@RequestBody BroadcastRequest request) {
        log.info("enter send BTC: {}", request);

        BroadcastResponse response = new BroadcastResponse();

        String rawTx = StringUtils.trimToNull(request.getRawTx());
        if (null == rawTx) {
            response.setError("Null raw tx");
        }

        try {
            String txid = btcService.broadcast(rawTx);
            response.setTxid(txid);
        } catch (Exception e) {
            e.printStackTrace();
            response.setError(e.getMessage());
        }

        log.info("exit send BTC: {}", response);
        return response;
    }

}
