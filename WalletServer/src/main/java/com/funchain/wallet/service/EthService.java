package com.funchain.wallet.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Configuration
@Service
@Slf4j
public class EthService {
    public static final long SEND_TIMEOUT = 30L;
    private static final long SLOW_NETWORK = 2000L;

    public static final int OK_CODE = 200;
    public static final int ERR_CODE = 500;

    @Value("${eth.node.url:http://47.96.67.121:8011}")
    public String ethNodeUrl;

    private OkHttpClient httpClient;

    @PostConstruct
    public void init() {
        OkHttpClient aHttpClient = new OkHttpClient();
        httpClient = aHttpClient.newBuilder()
                .connectTimeout(SEND_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(SEND_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    public String broadcast(String rawTx) throws Exception {
        String txid = this.sendRawTransaction(ethNodeUrl, rawTx);
        if (null == txid) {
            throw new Exception("Failed to broadcast the tx");
        }
        return txid;
    }

    public String sendRawTransaction(String webUrl, String rawTx) {
        log.info("send raw tx to {}", webUrl);
        long st = System.currentTimeMillis();

        HttpUrl url = HttpUrl.parse(webUrl);
        String jsonStr = "{\"method\":\"eth_sendRawTransaction\",\"params\":[\""
                + rawTx + "\"],\"id\":1,\"jsonrpc\":\"2.0\"}";

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonStr);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        String txid = null;
        try {
            Pair<String, Integer> res = sendRequest(httpClient, request);
            if (OK_CODE == res.getRight()) {
                JSONObject jsonObject = JSON.parseObject(res.getLeft());
                txid = jsonObject.getString("result");
                log.info("Send tx {} from  {}", txid, webUrl);
            }
        }
        catch (Throwable e) {
            log.error("Network error", e);
        } finally {
            long ct = System.currentTimeMillis() - st;
            if (SLOW_NETWORK < ct) {
                log.warn("SLow network: {}, {}ms", "sendRawTransaction", ct);
            }
        }

        log.info("send raw tx returns {}", txid);
        return txid;
    }

    public String getContractAddressByTx(String txid) {
        long st = System.currentTimeMillis();

        HttpUrl url = HttpUrl.parse(ethNodeUrl);
        String jsonStr = "{\"method\":\"eth_getTransactionByHash\",\"params\":[\""
                + txid + "\"],\"id\":1,\"jsonrpc\":\"2.0\"}";

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonStr);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        String contractAddr = null;
        try {
            Pair<String, Integer> res = sendRequest(httpClient, request);
            if (OK_CODE == res.getRight()) {
                JSONObject jsonObject = JSON.parseObject(res.getLeft());
                JSONObject resultObj = jsonObject.getJSONObject("result");

                contractAddr = resultObj.getString("creates");
                return contractAddr;
            }
        }
        catch (Throwable e) {
            log.error("Parse error", e);
        } finally {
            long ct = System.currentTimeMillis() - st;
            if (SLOW_NETWORK < ct) {
                log.warn("SLow network: {}, {}ms", "getTransactionByHash", ct);
            }
        }
        return contractAddr;
    }

    private Pair<String, Integer> sendRequest(OkHttpClient httpClient, Request request) {
        Response response = null;
        try {
            response = httpClient.newCall(request).execute();
            if (null == response) {
                log.error("Response error: {}", "NULL");
                return ImmutablePair.of(null, ERR_CODE);
            }

            if (OK_CODE != response.code()) {
                log.error("Response error: {}", response.code());
                return ImmutablePair.of(null, response.code());
            }

            String responseBody = response.body().string();
            return ImmutablePair.of(responseBody, OK_CODE);
        } catch (Throwable e) {
            log.error("HTTP error", e);
            return ImmutablePair.of(null, ERR_CODE);
        } finally {
            if (null != response) {
                response.close();
            }
        }
    }

}
