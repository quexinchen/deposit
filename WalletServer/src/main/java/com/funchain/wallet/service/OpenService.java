package com.funchain.wallet.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.funchain.wallet.model.BalanceElement;
import com.funchain.wallet.model.UtxoElement;
import lombok.extern.slf4j.Slf4j;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Open API
 *
 * @author Dev
 */
@Service
@Slf4j
public class OpenService {
    public static final long SEND_TIMEOUT = 30L;
    private static final long SLOW_NETWORK = 2000L;

    public static final int OK_CODE = 200;
    public static final int ERR_CODE = 500;

    @Value("${open.url:http://116.62.61.38:8890}")
    public String openUrl;

    @Value("${open.id:d50a8edd0a6644f88d37b57539d06ad6}")
    public String clientId;

    @Value("${open.secret:f5eecf068a565a33d2b7ca28517fdb653f93cb94}")
    public String clientCode;

    private OkHttpClient httpClient;

    @PostConstruct
    public void init() {
        OkHttpClient aHttpClient = new OkHttpClient();
        httpClient = aHttpClient.newBuilder()
                .connectTimeout(SEND_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(SEND_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    public List<BalanceElement> listTokenBalance() {
        String accessToken = login(openUrl);
        if (null == accessToken) {
            return Collections.emptyList();
        }
        return listTokenBalance(openUrl, accessToken);
    }

    public List<BalanceElement> listCoinBalance() {
        String accessToken = login(openUrl);
        if (null == accessToken) {
            return Collections.emptyList();
        }
        return listCoinBalance(openUrl, accessToken);
    }

    public List<UtxoElement> listUtxo() {
        String accessToken = login(openUrl);
        if (null == accessToken) {
            return Collections.emptyList();
        }
        return listUtxo(openUrl, accessToken);
    }

    public String login(String webUrl) {
        long st = System.currentTimeMillis();
        String url = webUrl + "/oauth/token";

        FormBody body = new FormBody.Builder()
                .add("grant_type", "client_credentials")
                .add("scope", "trust")
                .add("client_id", clientId)
                .add("client_secret", clientCode)
                .build();

        log.info("Login {} with {}:{}", url, clientId, clientCode);

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        String accessToken = null;
        try {
            Pair<String, Integer> res = sendRequest(httpClient, request);
            if(OK_CODE == res.getRight()) {
                JSONObject jsonObject = JSON.parseObject(res.getLeft());
                accessToken = jsonObject.getString("access_token");
            }
        } catch (Exception e) {
            log.error("json parse error", e);
        }
        finally {
            long ct = System.currentTimeMillis() - st;
            if (SLOW_NETWORK < ct) {
                log.warn("login costs {}", ct);
            }
        }
        return accessToken;
    }

    public List<BalanceElement> listTokenBalance(String webUrl, String accessToken) {
        long st = System.currentTimeMillis();
        String url = webUrl + "/v1/balance/token";

        FormBody body = new FormBody.Builder()
                .add("access_token", accessToken)
                .add("wallet_id", "2147483640")
                .add("coin_ref", "0")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        List<BalanceElement> balances = new ArrayList<>();
        try {
            Pair<String, Integer> res = sendRequest(httpClient, request);
            if(OK_CODE == res.getRight()) {
                JSONObject jsonObject = JSON.parseObject(res.getLeft());
                JSONArray jsonArray = jsonObject.getJSONArray("balance");
                for (Object obj : jsonArray) {
                    JSONObject json = (JSONObject) obj;
                    BalanceElement balance = new BalanceElement();
                    balance.setAddress(json.getString("address"));
                    balance.setAmount(json.getLongValue("amount"));
                    balances.add(balance);
                }
            }
        } catch (Exception e) {
            log.error("json parse error", e);
        }
        finally {
            long ct = System.currentTimeMillis() - st;
            if (SLOW_NETWORK < ct) {
                log.warn("login costs {}", ct);
            }
        }
        return balances;
    }

    public List<BalanceElement> listCoinBalance(String webUrl, String accessToken) {
        long st = System.currentTimeMillis();
        String url = webUrl + "/v1/balance/coin";

        FormBody body = new FormBody.Builder()
                .add("access_token", accessToken)
                .add("wallet_id", "2147483640")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        List<BalanceElement> balances = new ArrayList<>();
        try {
            Pair<String, Integer> res = sendRequest(httpClient, request);
            if(OK_CODE == res.getRight()) {
                JSONObject jsonObject = JSON.parseObject(res.getLeft());
                JSONArray jsonArray = jsonObject.getJSONArray("balance");
                for (Object obj : jsonArray) {
                    JSONObject json = (JSONObject) obj;
                    BalanceElement balance = new BalanceElement();
                    balance.setAddress(json.getString("address"));
                    balance.setAmount(json.getLongValue("amount"));
                    balances.add(balance);
                }
            }
        } catch (Exception e) {
            log.error("json parse error", e);
        }
        finally {
            long ct = System.currentTimeMillis() - st;
            if (SLOW_NETWORK < ct) {
                log.warn("login costs {}", ct);
            }
        }
        return balances;
    }

    public List<UtxoElement> listUtxo(String webUrl, String accessToken) {
        long st = System.currentTimeMillis();
        String url = webUrl + "/v1/balance/utxo";

        FormBody body = new FormBody.Builder()
                .add("access_token", accessToken)
                .add("wallet_id", "2147483640")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        List<UtxoElement> utxos = new ArrayList<>();
        try {
            Pair<String, Integer> res = sendRequest(httpClient, request);
            if(OK_CODE == res.getRight()) {
                JSONObject jsonObject = JSON.parseObject(res.getLeft());
                JSONArray jsonArray = jsonObject.getJSONArray("utxo");
                for (Object obj : jsonArray) {
                    JSONObject json = (JSONObject) obj;
                    UtxoElement utxo = new UtxoElement();
                    utxo.setTxid(json.getString("txid"));
                    utxo.setN(json.getIntValue("n"));
                    utxo.setScriptPubKey(json.getString("scriptPubKey"));
                    utxo.setAddress(json.getString("address"));
                    utxo.setAmount(json.getLongValue("amount"));
                    utxos.add(utxo);
                }
            }
        } catch (Exception e) {
            log.error("json parse error", e);
        }
        finally {
            long ct = System.currentTimeMillis() - st;
            if (SLOW_NETWORK < ct) {
                log.warn("login costs {}", ct);
            }
        }
        return utxos;
    }

    private Pair<String, Integer> sendRequest(OkHttpClient httpClient, Request request) {
        Response response = null;
        try {
            response = httpClient.newCall(request).execute();
            if (null == response) {
                log.error("Response error: {}", "NULL");
                return ImmutablePair.of(null, ERR_CODE);
            }

            if (OK_CODE != response.code()) {
                log.error("Response error: {}", response.code());
                return ImmutablePair.of(null, response.code());
            }

            String responseBody = response.body().string();
            return ImmutablePair.of(responseBody, OK_CODE);
        } catch (Throwable e) {
            log.error("HTTP error", e);
            return ImmutablePair.of(null, ERR_CODE);
        } finally {
            if (null != response) {
                response.close();
            }
        }
    }

}
