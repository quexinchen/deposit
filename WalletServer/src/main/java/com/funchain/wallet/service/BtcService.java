package com.funchain.wallet.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * BTC
 *
 * @author DEV
 */
@Service
@Slf4j
public class BtcService {
    public static final long SEND_TIMEOUT = 30L;
    private static final long SLOW_NETWORK = 2000L;

    public static final int OK_CODE = 200;
    public static final int ERR_CODE = 500;

    @Value("${btc.node.url:http://47.106.196.27:4001/insight-api}")
    public String btcNodeUrl;

    private OkHttpClient httpClient;

    @PostConstruct
    public void init() {
        OkHttpClient aHttpClient = new OkHttpClient();
        httpClient = aHttpClient.newBuilder()
                .connectTimeout(SEND_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(SEND_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    /**
     * 广播交易
     *
     * @param rawTx
     * @return
     */
    public String broadcast(String rawTx) throws Exception {
        String txid = this.sendRawTransaction(btcNodeUrl, rawTx);
        if (null == txid) {
            throw new Exception("Failed to broadcast the tx");
        }
        return txid;
    }

    public String sendRawTransaction(String webUrl, String rawtx) {
        long st = System.currentTimeMillis();

        String url = webUrl + "/tx/send";

        RequestBody body = new FormBody.Builder()
                .add("rawtx", rawtx)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        String txid = null;
        try {
            Pair<String, Integer> res = sendRequest(httpClient, request);
            if(OK_CODE == res.getRight()) {
                JSONObject jsonObject = JSON.parseObject(res.getLeft());
                txid = jsonObject.getString("txid");
            }
        } catch (Exception e) {
            log.error("json parse error", e);
        }
        finally {
            long ct = System.currentTimeMillis() - st;
            if (SLOW_NETWORK < ct) {
                log.warn("send tx costs {}", ct);
            }
        }
        return txid;
    }

    private Pair<String, Integer> sendRequest(OkHttpClient httpClient, Request request) {
        Response response = null;
        try {
            response = httpClient.newCall(request).execute();
            if (null == response) {
                log.error("Response error: {}", "NULL");
                return ImmutablePair.of(null, ERR_CODE);
            }

            if (OK_CODE != response.code()) {
                log.error("Response error: {}", response.code());
                return ImmutablePair.of(null, response.code());
            }

            String responseBody = response.body().string();
            return ImmutablePair.of(responseBody, OK_CODE);
        } catch (Throwable e) {
            log.error("HTTP error", e);
            return ImmutablePair.of(null, ERR_CODE);
        } finally {
            if (null != response) {
                response.close();
            }
        }
    }

}
