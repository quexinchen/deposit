package com.funchain.wallet;

import com.funchain.wallet.bip.BTCAddress;
import com.funchain.wallet.bip.ETHAddress;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class ETHAddressTest {

    @Test
    public void testAddress() {
        String pubKey = "036cc3f2dce26d752d58b642fff82dca99b92367a35c31c69e4aaa2b2d45928b50";
        String address = ETHAddress.toAccountAddress(pubKey);
        System.out.println(address);
    }

    @Test
    public void testAccount() throws Exception {
        String pubkeyFile = "dev/pubkey.eth.csv";
        List<KeyRecord> records = importKey(pubkeyFile);

        for (KeyRecord record : records) {
            String address = ETHAddress.toAccountAddress(record.getPubkey());
            System.out.println(address);
        }
    }

    @Test
    public void testToAccountAddress() throws Exception {
        String pubkeyFile = "prod/pubkey.eth.100.csv";

        List<KeyRecord> records = importKey(pubkeyFile);
        for (KeyRecord record : records) {
            String address = ETHAddress.toAccountAddress(record.getPubkey());

            StringBuilder sb = new StringBuilder();
            sb.append("insert into p_deposit_address(wallet_id,coin_type,address_index,address) ");
            sb.append("values(2147483641,60,");
            sb.append(record.getIndex()).append(",'").append(address).append("'");
            sb.append(");");
            System.out.println(sb.toString());
        }
    }

    public List<KeyRecord> importKey(String filePath) throws Exception {
        BufferedReader reader = null;
        FileReader fin = null;
        fin = new FileReader(filePath);
        reader = new BufferedReader(fin);

        List<KeyRecord> records = new ArrayList<>();

        String str = reader.readLine();
        while (null != str) {
            String[] strs = str.split(",");

            KeyRecord record = new KeyRecord();
            record.setIndex(strs[0].trim());
            record.setPubkey(strs[1].trim());

            records.add(record);

            str = reader.readLine();
        }
        return records;
    }

}
