package com.funchain.wallet;

import com.funchain.wallet.bip.HDWallet;
import org.bitcoinj.core.DumpedPrivateKey;
import org.bitcoinj.crypto.ChildNumber;
import org.bitcoinj.params.MainNetParams;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spongycastle.util.encoders.Hex;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

@RunWith(SpringRunner.class)
public class HDWalletTest {

    @Test
    public void testSeed1() {
        WalletArg walletArg = new WalletArg();
        walletArg.setWords(Arrays.asList(
                "army", "van", "defense", "carry",
                "jealous", "true", "garbage", "claim",
                "echo", "media", "make", "crunch"));
        walletArg.setPassphrase("");
        HDWallet.toSeed(walletArg);
        System.out.println(Hex.toHexString(walletArg.getSeed()));
        // 5b56c417303faa3fcba7e57400e120a0ca83ec5a4fc9ffba757fbe63fbd77a89a1a3be4c67196f57c39a88b76373733891bfaba16ed27a813ceed498804c0570
    }

    @Test
    public void testSeed2() {
        WalletArg walletArg = new WalletArg();
        walletArg.setWords(Arrays.asList(
                "army", "van", "defense", "carry",
                "jealous", "true", "garbage", "claim",
                "echo", "media", "make", "crunch"));
        walletArg.setPassphrase("SuperDuperSecret");
        HDWallet.toSeed(walletArg);
        System.out.println(Hex.toHexString(walletArg.getSeed()));
        // 3b5df16df2157104cfdd22830162a5e170c0161653e3afe6c88defeefb0818c793dbb28ab3ab091897d0715861dc8a18358f80b79d49acf64142ae57037d1d54
    }

    @Test
    public void testSeed3() {
        WalletArg walletArg = new WalletArg();
        walletArg.setWords(Arrays.asList("cake", "apple", "borrow", "silk",
                "endorse", "fitness", "top", "denial",
                "coil", "riot", "stay", "wolf",
                "luggage", "oxygen", "faint", "major",
                "edit", "measure", "invite", "love",
                "trap", "field", "dilemma", "oblige"));
        walletArg.setPassphrase("");
        HDWallet.toSeed(walletArg);
        System.out.println(Hex.toHexString(walletArg.getSeed()));
        // 3269bce2674acbd188d4f120072b13b088a0ecf87c6e4cae41657a0bb78f5315b33b3a04356e53d062e55f1e0deaa082df8d487381379df848a6ad7e98798404
    }

    @Test
    public void testFromPrivateKey() {
        String encoded = "L27rKoVgW4dYc1BxhD1X7jp9ixPGwnn3E2eggznmpYcuktDYPKaH";
        DumpedPrivateKey key = DumpedPrivateKey.fromBase58(MainNetParams.get(), encoded);
        System.out.println(key.getKey().getPrivateKeyAsHex());
    }

    @Test
    public void testXPub() {
        WalletArg wallet = new WalletArg();
        wallet.setWords(Arrays.asList(
                "october", "clutch", "ticket", "mistake",
                "naive", "fortune", "tongue", "vague",
                "extra", "exercise", "vault", "wolf"));
        wallet.setPassphrase("");

        HDWallet.toSeed(wallet);
        HDWallet.toMaster(wallet);

        ExportArg export = new ExportArg();
        export.setPath(Arrays.asList(
                new ChildNumber(44, true),
                new ChildNumber(0, true),
                new ChildNumber(0, true)
                ));

        String xPub = HDWallet.toXPub(wallet, export);
        System.out.println(xPub);
        // xpub6C7oT4UaKxGnwAko5nseX7t8BXREG5TRMYtwTT6BZy9TLcnAR7mtE5bomueLgJvUrbW45UmZ1EJv4fLoyRfuQdaZdZL4CPqb32qm8X2UCrm

        export.setPath(Arrays.asList(
                new ChildNumber(44, true),
                new ChildNumber(60, true),
                new ChildNumber(0, true)
        ));

        xPub = HDWallet.toXPub(wallet, export);
        System.out.println(xPub);
        // xpub6BkykKz7UnWMVFq5U2KU2qdn83fZkM3mU8iMvB9AbeuAy5qWaGWiVEj8Ceu1hG5ZDRqf4YEmqGJRDTed4fYSYK66QJTX8N6QgaDtgxNFo2w
    }

}
