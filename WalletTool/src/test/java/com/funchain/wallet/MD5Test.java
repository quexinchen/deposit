package com.funchain.wallet;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.MessageDigest;

@RunWith(SpringRunner.class)
public class MD5Test {

    @Test
    public void testMD5() throws Exception {
        MessageDigest pubkeyMd5 = MessageDigest.getInstance("MD5");
        MessageDigest prvkeyMd5 = MessageDigest.getInstance("MD5");

        System.out.println(pubkeyMd5.toString());
        System.out.println(prvkeyMd5.toString());
        System.out.println(pubkeyMd5.equals(prvkeyMd5));

        pubkeyMd5.update("abc".getBytes());
        prvkeyMd5.update("123".getBytes());

        byte[] pubkey = pubkeyMd5.digest();
        byte[] prvkey = prvkeyMd5.digest();
        System.out.println(Hex.encodeHexString(pubkey));
        System.out.println(Hex.encodeHexString(prvkey));
    }

}
