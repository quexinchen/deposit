package com.funchain.wallet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class PasswordUtilsTest {

    @Test
    public void testPassword() {
        String p = "";
        System.out.println(p + ":" + PasswordUtils.checkPassword(p));

        p = "1";
        System.out.println(p + ":" + PasswordUtils.checkPassword(p));

        p = "12345678";
        System.out.println(p + ":" + PasswordUtils.checkPassword(p));

        p = "1234abcd";
        System.out.println(p + ":" + PasswordUtils.checkPassword(p));

        p = "1234abCD";
        System.out.println(p + ":" + PasswordUtils.checkPassword(p));

        p = "1234abc$";
        System.out.println(p + ":" + PasswordUtils.checkPassword(p));

        p = "1234abC$";
        System.out.println(p + ":" + PasswordUtils.checkPassword(p));

        p = "!Abc123456";
        System.out.println(p + ":" + PasswordUtils.checkPassword(p));
    }

}
