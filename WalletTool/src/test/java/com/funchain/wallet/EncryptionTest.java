package com.funchain.wallet;

import com.funchain.wallet.bip.Encryption;
import org.bouncycastle.util.encoders.Hex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class EncryptionTest {

    @Test
    public void testEncryption() {
        String password = "123456789";
        byte[] key = Encryption.encodePassword(password);
        System.out.println(key.length);

        String plain = "DFCBE266E811A36C71DEC1518BBB28F5411623946A445CB2354FE4E30DEC54B2";
        byte[] data = Encryption.encrypt(key, Hex.decode(plain));
        System.out.println(Hex.toHexString(data));
        // 706faca7ba65c3e39f5275bf29fbc12866a750dc86328a632d71182be2f5e250

        byte[] text = Encryption.decrypt(key, data);
        System.out.println(Hex.toHexString(text));
    }

    @Test
    public void testDecryption() {
        String password = "$Abcd123";
        byte[] key = Encryption.encodePassword(password);

        String hex = "9d765dde5387c6e9fe16fcea8aa2230de0899409aeb55b6953e5702a21dac8d9874685e4a3449abf57f14a535770fe18";
        byte[] text = Encryption.decrypt(key, Hex.decode(hex));
        System.out.println(Hex.toHexString(text));
    }

}
