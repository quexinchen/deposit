package com.funchain.wallet;

import com.funchain.wallet.bip.Encryption;
import com.funchain.wallet.bip.HDWallet;
import org.bitcoinj.crypto.ChildNumber;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

@RunWith(SpringRunner.class)
public class ExportActionTest {

    @Test
    public void testExport() throws Exception {
        WalletArg wallet = new WalletArg();
        wallet.setWords(Arrays.asList(
                "october", "clutch", "ticket", "mistake",
                "naive", "fortune", "tongue", "vague",
                "extra", "exercise", "vault", "wolf"));
        wallet.setPassphrase("");

        HDWallet.toSeed(wallet);
        HDWallet.toMaster(wallet);

        String password = "123456789";
        wallet.setEncodedPassword(Encryption.encodePassword(password));

        ExportArg export = new ExportArg();
        export.setPathStr("44'/0'/0'/0");
        export.setPath(Arrays.asList(
                new ChildNumber(44, true),
                new ChildNumber(0, true),
                new ChildNumber(0, true),
                new ChildNumber(0, false)
        ));
        export.setStartStr("30");
        export.setStart(new ChildNumber(30));
        export.setSkip(30);
        export.setNumber(5);
        export.setEnd(new ChildNumber(30 + 30 * 4));

        ExportAction.export(wallet, export);
    }

}
