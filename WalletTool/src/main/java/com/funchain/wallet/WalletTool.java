package com.funchain.wallet;

import com.beust.jcommander.JCommander;
import com.funchain.wallet.bip.Dictionary;
import com.funchain.wallet.bip.Encryption;
import com.funchain.wallet.bip.HDWallet;
import org.bitcoinj.crypto.ChildNumber;

import java.util.*;

public class WalletTool {
    private WalletArg walletArg = new WalletArg();

    private void xpub(String[] parameters) {
        if (parameters == null || parameters.length != 1) {
            System.out.println("xpub needs 1 parameter like the following: ");
            System.out.println("xpub 44'/0'/0'");
            System.out.println("xpub 44'/60'/0'");
            System.out.println(" ");
            return;
        }

        ExportArg exportArg = new ExportArg();

        List<ChildNumber> pathList = new ArrayList<>();
        String pathStr = parameters[0];
        try {
            String[] strs = pathStr.split("/");
            for (String aStr : strs) {
                aStr = aStr.trim();
                if (aStr.endsWith("'")) {
                    int idx = Integer.parseInt(aStr.substring(0, aStr.length() - 1));
                    pathList.add(new ChildNumber(idx, true));
                } else {
                    pathList.add(new ChildNumber(Integer.parseInt(aStr)));
                }
            }
            exportArg.setPath(pathList);
        } catch (Exception e) {
            System.out.println("Wrong path!");
            return;
        }

        try {
            String filePath = ExportAction.xpub(walletArg, exportArg);
            System.out.println("xPubKey exported in file: " + filePath);
        } catch (Exception e) {
            System.out.println("Failed to export the xPubKey!");
        }
        System.out.println(" ");
    }

    private void export(String[] parameters) {
        if (parameters == null || parameters.length != 4) {
            System.out.println("export needs 4 parameters like the following: ");
            System.out.println("export 44'/0'/0'/0 30 30 10");
            System.out.println("export 44'/60'/0'/0 0 1 1");
            System.out.println(" ");
            return;
        }

        ExportArg exportArg = new ExportArg();

        List<ChildNumber> pathList = new ArrayList<>();
        String pathStr = parameters[0];
        exportArg.setPathStr(pathStr);
        try {
            String[] strs = pathStr.split("/");
            for (String aStr : strs) {
                aStr = aStr.trim();
                if (aStr.endsWith("'")) {
                    int idx = Integer.parseInt(aStr.substring(0, aStr.length() - 1));
                    pathList.add(new ChildNumber(idx, true));
                } else {
                    pathList.add(new ChildNumber(Integer.parseInt(aStr)));
                }
            }
            exportArg.setPath(pathList);
        } catch (Exception e) {
            System.out.println("Wrong path!");
            return;
        }

        try {
            String startStr = parameters[1].trim();
            String skipStr = parameters[2].trim();
            String numberStr = parameters[3].trim();

            int start = Integer.parseInt(startStr);
            int skip = Integer.parseInt(skipStr);
            int number = Integer.parseInt(numberStr);
            int end = start + skip * (number - 1);

            exportArg.setStartStr(startStr);
            exportArg.setSkip(skip);
            exportArg.setNumber(number);

            exportArg.setStart(new ChildNumber(start));
            exportArg.setEnd(new ChildNumber(end));
        } catch (Exception e) {
            System.out.println("Wrong start or end!");
            return;
        }

        try {
            ExportAction.export(walletArg, exportArg);
        } catch (Exception e) {
            System.out.println("Failed to export the private keys!");
            e.printStackTrace();
        }
        System.out.println(" ");
    }

    private void help() {
        System.out.println("Help: List of WalletArg Tool commands");
        System.out.println("xpub");
        System.out.println("export");
        System.out.println("quit");
        System.out.println("Input any one of the listed commands, to display how-to tips.");
        System.out.println(" ");
    }

    private String[] getCmd(String cmdLine) {
        if (cmdLine.isEmpty()) {
            return cmdLine.split("\\s+");
        }
        String[] strArray = cmdLine.split("\"");
        int num = strArray.length;
        int start = 0;
        int end = 0;
        if (cmdLine.charAt(0) == '\"') {
            start = 1;
        }
        if (cmdLine.charAt(cmdLine.length() - 1) == '\"') {
            end = 1;
        }
        if (((num + end) & 1) == 0) {
            return new String[]{"ErrorInput"};
        }

        List<String> cmdList = new ArrayList<>();
        for (int i = start; i < strArray.length; i++) {
            if ((i & 1) == 0) {
                cmdList.addAll(Arrays.asList(strArray[i].trim().split("\\s+")));
            } else {
                cmdList.add(strArray[i].trim());
            }
        }
        Iterator ito = cmdList.iterator();
        while (ito.hasNext()) {
            if (ito.next().equals("")) {
                ito.remove();
            }
        }
        String[] result = new String[cmdList.size()];
        return cmdList.toArray(result);
    }

    private void run() {
        Scanner in = new Scanner(System.in);

        this.init(in);

        while (in.hasNextLine()) {
            String cmd = "";
            try {
                String cmdLine = in.nextLine().trim();
                String[] cmdArray = getCmd(cmdLine);
                // split on trim() string will always return at the minimum: [""]
                cmd = cmdArray[0];
                if ("".equals(cmd)) {
                    continue;
                }
                String[] parameters = Arrays.copyOfRange(cmdArray, 1, cmdArray.length);
                String cmdLowerCase = cmd.toLowerCase();

                switch (cmdLowerCase) {
                    case "help": {
                        help();
                        break;
                    }
                    case "xpub": {
                        xpub(parameters);
                        break;
                    }
                    case "export": {
                        export(parameters);
                        break;
                    }
                    case "exit":
                    case "quit": {
                        System.out.println("Exit !!!");
                        return;
                    }
                    default: {
                        System.out.println("Invalid cmd: " + cmd);
                        help();
                    }
                }
            } catch (Exception e) {
                System.out.println(cmd + " failed!");
                e.printStackTrace();
            }
        }
    }

    private void init(Scanner in) {
        System.out.println(" ");
        System.out.println("Welcome to WalletArg Tool");
        System.out.println(" ");

        // Number of mnemonic words
        String str;
        Map<String, Integer> wordMap = new HashMap<>();
        while (true) {
            System.out.println(
                    "Number of mnemonic words(12/24): ");
            str = in.nextLine().trim();

            int wordCount = 0;
            try {
                wordCount = Integer.parseInt(str);
            } catch (NumberFormatException e) {
                System.out.println("Not a number!");
                continue;
            }

            if (!(12 == wordCount || 24 == wordCount)) {
                System.out.println("Wrong number!");
                continue;
            }

            for (int i = 1; i <= wordCount; i++) {
                wordMap.put("Word #" + i + ": ", i);
            }
            break;
        }

        // Generate
        System.out.println(" ");
        Map<Integer, String> words = new TreeMap<>();
        while (true) {
            List<String> tmpWords = com.funchain.wallet.bip.Dictionary.generateWords(wordMap.size());
            StringBuilder sb = new StringBuilder();
            int i = 1;
            for (String tw : tmpWords) {
                words.put(i, tw);
                sb.append(tw).append(" ");
                i++;
            }
            System.out.println("Sample mnemonic words: ");
            System.out.println(sb.toString());
            System.out.println("More sample mnemonic words? (y/n): ");
            str = in.nextLine().trim();
            if ("y".equalsIgnoreCase(str)) {
                continue;
            }
            break;
        }

        // Mnemonic words
        while (true) {
            System.out.println(" ");
            System.out.println("Import mnemonic words: ");
            for (Map.Entry<String, Integer> wordEntry : wordMap.entrySet()) {
                while (true) {
                    System.out.println(wordEntry.getKey());
                    str = in.nextLine().trim().toLowerCase();

                    if (!com.funchain.wallet.bip.Dictionary.checkWord(str)) {
                        System.out.println("Not a word!");
                        continue;
                    }

                    words.put(wordEntry.getValue(), str);
                    break;
                }
            }
            walletArg.setWords(new ArrayList<>(words.values()));
            if (!Dictionary.checkWords(walletArg.getWords())) {
                System.out.println("Wrong mnemonic words!");
                continue;
            }
            break;
        }

        // Passphrase
        System.out.println(" ");
        walletArg.setPassphrase("");
        while (true) {
            System.out.println(
                    "Passphrase (type n if no passphrase): ");
            str = in.nextLine().trim();
            if (str.isEmpty()) {
                continue;
            }
            if ("n".equalsIgnoreCase(str)) {
                break;
            }
            walletArg.setPassphrase(str);
            break;
        }

        // Password
        System.out.println(" ");
        while (true) {
            System.out.println(
                    "Password: ");
            str = in.nextLine().trim();
            if (str.isEmpty() || !PasswordUtils.checkPassword(str)) {
                System.out.println("Weak password!");
                continue;
            }
            walletArg.setPassword(str);

            while (true) {
                System.out.println(
                        "Password again: ");
                str = in.nextLine().trim();
                if (str.isEmpty()) {
                    continue;
                }
                break;
            }

            if (!str.equals(walletArg.getPassword())) {
                System.out.println("Passwords don't match!");
                walletArg.setPassword(null);
                continue;
            }

            break;
        }

        HDWallet.toSeed(walletArg);
        walletArg.setWords(null);
        walletArg.setPassphrase(null);
        HDWallet.toMaster(walletArg);
        walletArg.setSeed(null);

        walletArg.setEncodedPassword(Encryption.encodePassword(walletArg.getPassword()));
        walletArg.setPassword(null);

        // help
        System.out.println(" ");
        this.help();
    }

    public static void main(String[] args) {
        WalletTool cli = new WalletTool();

        JCommander.newBuilder()
                .addObject(cli)
                .build()
                .parse(args);

        cli.run();
    }
}
