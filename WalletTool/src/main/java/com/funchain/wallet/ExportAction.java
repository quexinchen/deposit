package com.funchain.wallet;

import com.alibaba.fastjson.JSON;
import com.funchain.wallet.bip.HDWallet;
import org.apache.commons.io.IOUtils;
import org.bitcoinj.crypto.ChildNumber;
import org.spongycastle.util.encoders.Hex;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.security.MessageDigest;

/**
 * Export action
 *
 * @author Dev
 */
public class ExportAction {

    /**
     * 导出母公钥
     *
     * @param wallet
     * @param export
     * @return
     * @throws Exception
     */
    public static String xpub(WalletArg wallet, ExportArg export) throws Exception {
        String filePath = "xpub" + System.currentTimeMillis() + ".csv";
        BufferedWriter writer = null;
        FileWriter out = new FileWriter(filePath);
        writer = new BufferedWriter(out);

        String xpub = HDWallet.toXPub(wallet, export);
        IOUtils.write(xpub + IOUtils.LINE_SEPARATOR, writer);

        writer.flush();
        writer.close();

        return filePath;
    }

    /**
     * 导出私钥
     *
     * @param walletArg
     * @param exportArg
     * @return
     * @throws Exception
     */
    public static void export(WalletArg walletArg, ExportArg exportArg) throws Exception {
        String orderId = "" + System.currentTimeMillis();

        String pubkeyFile = "pubkey." + orderId + ".csv";
        String prvkeyFile = "prvkey." + orderId + ".csv";
        String descFile = "desc." + orderId + ".csv";

        BufferedWriter pubkeyWriter = new BufferedWriter(new FileWriter(pubkeyFile));
        BufferedWriter prvkeyWriter = new BufferedWriter(new FileWriter(prvkeyFile));

        MessageDigest pubkeyMd = MessageDigest.getInstance("MD5");
        MessageDigest prvkeyMd = MessageDigest.getInstance("MD5");

        HDWallet.toParent(walletArg, exportArg);
        int idx = exportArg.getStart().i();
        int end = exportArg.getEnd().i();
        int skip = exportArg.getSkip();
        while (idx <= end) {
            KeyRecord record = HDWallet.toChild(walletArg, exportArg, new ChildNumber(idx));

            String publine = record.getIndex() + "," + record.getPubkey() + IOUtils.LINE_SEPARATOR;
            String prvline = record.getIndex() + "," + record.getPubkey() + "," + record.getPrvkey() + IOUtils.LINE_SEPARATOR;

            IOUtils.write(publine, pubkeyWriter);
            IOUtils.write(prvline, prvkeyWriter);

            pubkeyMd.update(publine.getBytes());
            prvkeyMd.update(prvline.getBytes());

            idx += skip;
        }

        pubkeyWriter.flush();
        prvkeyWriter.flush();
        pubkeyWriter.close();
        prvkeyWriter.close();

        String pubkeyMd5 = Hex.toHexString(pubkeyMd.digest());
        String prvkeyMd5 = Hex.toHexString(prvkeyMd.digest());

        ExportDesc desc = new ExportDesc();
        desc.setTimestamp(Long.parseLong(orderId));
        desc.setSeed(walletArg.getSeedChecksum());
        desc.setPath(exportArg.getPathStr());
        desc.setStart(exportArg.getStart().getI());
        desc.setEnd(exportArg.getEnd().getI());
        desc.setSkip(exportArg.getSkip());
        desc.setPubkeymd5(pubkeyMd5);
        desc.setPrvkeymd5(prvkeyMd5);

        String descJson = JSON.toJSONString(desc);

        BufferedWriter descWriter = new BufferedWriter(new FileWriter(descFile));
        IOUtils.write(descJson, descWriter);
        descWriter.flush();
        descWriter.close();

        System.out.println(descFile);
        System.out.println(pubkeyFile);
        System.out.println(prvkeyFile);

        return;
    }

}
