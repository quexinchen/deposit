package com.funchain.wallet;

import org.apache.commons.lang3.ArrayUtils;

import java.io.Console;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

public class PasswordUtils {

    public static boolean checkPassword(String password) {
        String policy = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}";
        return Pattern.matches(policy, password);
    }

    public static char[] inputPassword() throws IOException {
        char[] password;
        Console cons = System.console();
        while (true) {
            if (cons != null) {
                password = cons.readPassword("Password: ");

            } else {
                System.out.println("NULL CONSOLE");

                byte[] passwd0 = new byte[64];
                int len = System.in.read(passwd0, 0, passwd0.length);
                int i;
                for (i = 0; i < len; i++) {
                    if (passwd0[i] == 0x09 || passwd0[i] == 0x0A) {
                        break;
                    }
                }
                byte[] passwd1 = Arrays.copyOfRange(passwd0, 0, i);

                password = byte2Char(passwd1);
                clear(passwd0);
                clear(passwd1);
            }

            if (password != null && password.length > 0) {
                return password;
            }

            clear(password);
            System.out.println("Invalid password, please input again.");
        }
    }

    /**
     * utf-8 bytes to chars
     */
    public static char[] byte2Char(byte[] a) {
        int len = 0;
        for (int i = 0; i < a.length; ) {
            byte b = a[i];
            if ((b & 0x80) == 0) {
                i++;  // 0xxxxxxx
            } else if ((b & 0xE0) == 0xC0) {
                i += 2; // 110xxxxx 10xxxxxx
            } else if ((b & 0xF0) == 0xE0) {
                i += 3; // 1110xxxx 10xxxxxx 10xxxxxx
            } else {
                i++;  // unsupport
            }
            len++;
        }

        char[] result = new char[len];
        int j = 0;
        for (int i = 0; i < a.length; ) {
            byte b = a[i];
            if ((b & 0x80) == 0) {
                i++;
                result[j++] = (char) b; // 0xxxxxxx
                continue;
            }
            if ((b & 0xE0) == 0xC0 && a.length - i >= 2) {
                result[j++] = (char) ((a[i + 1] & 0x3F) | (b & 0x1F) << 6);
                i += 2;
                continue;
            }
            if ((b & 0xF0) == 0xE0 && a.length - i >= 2) {
                result[j++] = (char) ((a[i + 2] & 0x3F) | ((a[i + 1] & 0x3F) << 6) | ((b & 0x0F) << 12));
                i += 3;
                continue;
            }
            i++;
            result[j++] = (char) b; // other
        }
        return result;
    }

    /**
     * char to utf-8 bytes
     */
    public static byte[] char2Byte(char[] a) {
        int len = 0;
        for (char c : a) {
            if (c > 0x7FF) {
                len += 3;
            } else if (c > 0x7F) {
                len += 2;
            } else {
                len++;
            }
        }

        byte[] result = new byte[len];
        int i = 0;
        for (char c : a) {
            if (c > 0x7FF) {
                result[i++] = (byte) (((c >> 12) & 0x0F) | 0xE0);
                result[i++] = (byte) (((c >> 6) & 0x3F) | 0x80);
                result[i++] = (byte) ((c & 0x3F) | 0x80);
            } else if (c > 127) {
                result[i++] = (byte) (((c >> 6) & 0x1F) | 0xC0);
                result[i++] = (byte) ((c & 0x3F) | 0x80);
            } else {
                result[i++] = (byte) (c & 0x7F);
            }
        }
        return result;
    }

    public static void clear(char[] a) {
        if (ArrayUtils.isEmpty(a)) {
            return;
        }
        for (int i = 0; i < a.length; i++) {
            a[i] = 0;
        }
    }

    public static void clear(byte[] a) {
        if (ArrayUtils.isEmpty(a)) {
            return;
        }
        for (int i = 0; i < a.length; i++) {
            a[i] = 0;
        }
    }

}
