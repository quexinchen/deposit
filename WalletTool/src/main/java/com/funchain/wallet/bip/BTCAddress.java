package com.funchain.wallet.bip;

import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Utils;
import org.bitcoinj.crypto.DeterministicKey;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;

/**
 * 生成BTC地址
 *
 * @author Dev
 */
public class BTCAddress {

    public static String toP2PKHAddress(int network, String pubKey) {
        ECKey key = DeterministicKey.fromPublicOnly(Utils.HEX.decode(pubKey));

        NetworkParameters parameters = network == 0 ? MainNetParams.get() : TestNet3Params.get();
        return key.toAddress(parameters).toBase58();
    }

}
