package com.funchain.wallet.bip;

import com.funchain.wallet.ExportArg;
import com.funchain.wallet.KeyRecord;
import com.funchain.wallet.WalletArg;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.crypto.ChildNumber;
import org.bitcoinj.crypto.DeterministicKey;
import org.bitcoinj.crypto.HDKeyDerivation;
import org.bitcoinj.crypto.MnemonicCode;
import org.bitcoinj.params.MainNetParams;
import org.spongycastle.util.encoders.Hex;

public class HDWallet {

    /**
     * From mnemonic words to seed
     *
     * @param walletArg
     */
    public static void toSeed(WalletArg walletArg) {
        byte[] seed = MnemonicCode.toSeed(walletArg.getWords(), walletArg.getPassphrase());
        walletArg.setSeed(seed);

        byte[] hash = Sha256Hash.hash(Sha256Hash.hash(seed));
        walletArg.setSeedChecksum(Hex.toHexString(hash).substring(0, 8));
    }

    /**
     * From seed to master key
     *
     * @param walletArg
     */
    public static void toMaster(WalletArg walletArg) {
        DeterministicKey master = HDKeyDerivation.createMasterPrivateKey(walletArg.getSeed());
        walletArg.setM(master);
    }

    /**
     * Derive parent key
     *
     * @param walletArg
     * @param exportArg
     */
    public static void toParent(WalletArg walletArg, ExportArg exportArg) {
        DeterministicKey p = walletArg.getM();
        for (ChildNumber c : exportArg.getPath()) {
            p = HDKeyDerivation.deriveChildKey(p, c);
        }
        exportArg.setParent(p);
    }

    /**
     * Derive child key
     *
     * @param walletArg
     * @param exportArg
     * @param c
     * @return
     */
    public static KeyRecord toChild(WalletArg walletArg, ExportArg exportArg, ChildNumber c) {
        DeterministicKey key = HDKeyDerivation.deriveChildKey(exportArg.getParent(), c);
        String pubKey = key.getPublicKeyAsHex();

        byte[] encryptedPrivKey = Encryption.encrypt(walletArg.getEncodedPassword(), key.getPrivKeyBytes());
        String prvKey = Hex.toHexString(encryptedPrivKey);

        KeyRecord record = new KeyRecord();
        record.setIndex(c.toString());
        record.setPubkey(pubKey);
        record.setPrvkey(prvKey);
        return record;
    }

    /**
     * Derive the Extended Public Key
     *
     * @param wallet
     * @param export
     * @return
     */
    public static String toXPub(WalletArg wallet, ExportArg export) {
        DeterministicKey p = wallet.getM();
        for (ChildNumber c : export.getPath()) {
            p = HDKeyDerivation.deriveChildKey(p, c);
        }

        NetworkParameters network = MainNetParams.get();
        return p.serializePubB58(network);
    }

}
