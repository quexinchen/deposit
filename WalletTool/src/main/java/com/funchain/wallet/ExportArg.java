package com.funchain.wallet;

import lombok.Data;
import org.bitcoinj.crypto.ChildNumber;
import org.bitcoinj.crypto.DeterministicKey;

import java.util.List;

@Data
public class ExportArg {

    private String pathStr;
    private String startStr;

    private List<ChildNumber> path;

    private ChildNumber start;

    private ChildNumber end;

    private int skip;
    private int number;

    private DeterministicKey parent;

}
