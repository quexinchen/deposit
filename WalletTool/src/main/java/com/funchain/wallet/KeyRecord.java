package com.funchain.wallet;

import lombok.Data;

/**
 * 一个地址
 *
 * @author Dev
 */
@Data
public class KeyRecord {

    private String index;

    private String pubkey;

    private String prvkey;

}
