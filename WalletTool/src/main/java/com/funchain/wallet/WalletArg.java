package com.funchain.wallet;

import lombok.Data;
import org.bitcoinj.crypto.DeterministicKey;

import java.util.List;

@Data
public class WalletArg {

    private List<String> words;

    private String passphrase;

    private byte[] seed;

    private String seedChecksum;

    private DeterministicKey m;

    private String password;

    private byte[] encodedPassword;

}
