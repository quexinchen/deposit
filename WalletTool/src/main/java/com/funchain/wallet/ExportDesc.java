package com.funchain.wallet;

import lombok.Data;

/**
 * 导出的描述信息
 *
 * @author Dev
 */
@Data
public class ExportDesc {

    private long timestamp;

    private String seed;

    private String path;

    private int start;

    private int end;

    private int skip;

    private String pubkeymd5;

    private String prvkeymd5;

}
